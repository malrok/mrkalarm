package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.models.Music;

import java.util.ArrayList;
import java.util.List;

public class MusicListViewModel extends AbstractListViewModel<Music> {

	public MusicListViewModel() {
		super();
	}

	private MusicListViewModel(Parcel in) {
		this.loadFromData(in.readParcelableArray(Alarm.class.getClassLoader()), MusicViewModel.class);
	}

	@Override
	public IListViewModel<Music> copy() {
		MusicListViewModel musics = new MusicListViewModel();

		for (Music music : this.getDataList()) {
			MusicViewModel vm = new MusicViewModel();
			vm.loadFromData(music.copy());
			musics.getViewModelList().add(vm);
		}

		return musics;
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Music> dataLoader) {
		// nothing to do
	}

	public void loadFromData(List<Music> dataList) {
		clear();

		for (Music data : dataList) {
			MusicViewModel vm = new MusicViewModel();
			vm.loadFromData(data);
			this.getViewModelList().add(vm);
		}
	}

//	public void addMusicViewModel(MusicViewModel musicVm) {
//		this.getViewModelList().add(musicVm);
//	}

	public boolean songSelected(String song) {
		boolean ret = false;

		if (!getViewModelList().isEmpty()) {
			for (IViewModel<Music> musicVM : this.getViewModelList()) {
				ret = ret || musicVM.getData().getPath().equalsIgnoreCase(song);
			}
		}

		return ret;
	}

//	public void addSongToMusicList(String path, String artist, String title, Long albumId, boolean isFile, int type) {
//		if (!songSelected(path)) {
//			this.getViewModelList().add(new MusicViewModel(path, artist, title, albumId, isFile, type));
//		}
//	}

	public void removeSongFromMusicList(String path) {
		ArrayList<IViewModel<Music>> list = new ArrayList<>();
		for (IViewModel<Music> musicVM : this.getViewModelList()) {
			if (musicVM.getData().getPath().equalsIgnoreCase(path))
				list.add(musicVM);
		}
		this.getViewModelList().removeAll(list);
	}

//	public void removeSoundFromMusicList() {
//		ArrayList<IViewModel<Music>> list = new ArrayList<>();
//		for (IViewModel<Music> musicVM : this.getViewModelList()) {
//			if (musicVM.getData().getType() == 0)
//				list.add(musicVM);
//		}
//		this.getViewModelList().removeAll(list);
//	}
//
//	public void removePlaylistFromMusicList() {
//		ArrayList<IViewModel<Music>> list = new ArrayList<>();
//		for (IViewModel<Music> musicVM : this.getViewModelList()) {
//			if (musicVM.getData().getType() == 2)
//				list.add(musicVM);
//		}
//		this.getViewModelList().removeAll(list);
//	}
//
//	public String getTone(int alarmType) {
//		String ret;
//
//		if (alarmType == 0) {
//			ret = getNotification();
//		} else if (alarmType == 1) {
//			ret = new AlarmSoundHelper(this.getViewModelList(), alarmType).getMusic();
//		} else {
//			ret = getPlaylist();
//		}
//
//		return ret;
//	}
//
//	public String getToneName(int alarmType) {
//		String ret = "";
//
//		if (alarmType == 0) {
//			ret = getNotificationName();
//		} else if (alarmType == 1) {
//			ret = getMusicName(-1, alarmType);
//		} else {
//			ret = getPlaylistName();
//		}
//
//		return ret;
//	}
//
//	public String getNotification() {
//		String ret = "";
//
//		for (IViewModel<Music> musicItem : this.getViewModelList()) {
//			if (musicItem.getData().getType() == 0 && musicItem.getData().isFile())
//				ret = musicItem.getData().getPath();
//		}
//
//		return ret;
//	}
//
//	public String getPlaylist() {
//		String ret = "";
//
//		for (IViewModel<Music> musicItem : this.getViewModelList()) {
//			if (musicItem.getData().getType() == 2 && musicItem.getData().isFile())
//				ret = AlarmSoundHelper.getMusicFromPlaylist(musicItem.getData().getPath());
//		}
//
//		return ret;
//	}
//
//	public String getNotificationName() {
//		String ret = null;
//
//		for (IViewModel<Music> musicItem : this.getViewModelList()) {
//			if (musicItem.getData().getType() == 0 && musicItem.getData().isFile())
//				ret = musicItem.getData().getName();
//		}
//
//		return ret;
//	}
//
//	public String getPlaylistName() {
//		String ret = null;
//
//		for (IViewModel<Music> musicItem : this.getViewModelList()) {
//			if (musicItem.getData().getType() == 2 && musicItem.getData().isFile())
//				ret = musicItem.getData().getName();
//		}
//
//		return ret;
//	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public MusicListViewModel createFromParcel(Parcel in) {
			return new MusicListViewModel(in);
		}

		public MusicListViewModel[] newArray(int size) {
			return new MusicListViewModel[size];
		}
	};
}
