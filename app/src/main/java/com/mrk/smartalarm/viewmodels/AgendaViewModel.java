package com.mrk.smartalarm.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.smartalarm.models.Agenda;

public class AgendaViewModel extends AbstractViewModel<Agenda> {

	private RulesListViewModel rulesList;

	public AgendaViewModel() {
		super();
		this.data = new Agenda();

		this.getData().setIdentifier(IdentifierHelper.getInstance().getIdentifierForClass(Agenda.class));

		this.rulesList = new RulesListViewModel();
	}

	private AgendaViewModel(Parcel in) {
		this.loadFromData((Agenda) in.readParcelable(Agenda.class.getClassLoader()));
	}

	@Override
	public AgendaViewModel copy() {
		AgendaViewModel newVM = new AgendaViewModel();

		newVM.loadFromData(this.data);

		return newVM;
	}

	public RulesListViewModel getRulesList() {
		return this.rulesList;
	}

	public RuleViewModel getRuleFromEvent(String entryName) {
		return this.rulesList.getRuleForEntryName(entryName);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public AgendaViewModel createFromParcel(Parcel in) {
			return new AgendaViewModel(in);
		}

		public AgendaViewModel[] newArray(int size) {
			return new AgendaViewModel[size];
		}
	};
}
