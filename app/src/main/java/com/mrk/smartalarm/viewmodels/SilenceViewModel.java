package com.mrk.smartalarm.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.smartalarm.models.Silence;

public class SilenceViewModel extends AbstractViewModel<Silence> {

	public SilenceViewModel() {
		super();
		this.data = new Silence();
		this.data.setIdentifier(IdentifierHelper.getInstance().getIdentifierForClass(Silence.class));
	}

	private SilenceViewModel(Parcel in) {
		this.loadFromData((Silence) in.readParcelable(Silence.class.getClassLoader()));
	}

	@Override
	public SilenceViewModel copy() {
		SilenceViewModel newVM = new SilenceViewModel();

		newVM.loadFromData(this.data);

		return newVM;
	}

	public int getBeginMinutes() {
		if (getData().getBeginTime() > 0) {
			return getData().getBeginTime() % 60;
		} else {
			return (1440 + getData().getBeginTime()) % 60;
		}
	}

	public int getBeginHour() {
		if (getData().getBeginTime() > 0) {
			return getData().getBeginTime() / 60;
		} else {
			return (1440 + getData().getBeginTime()) / 60;
		}
	}

	public int getEndMinutes() {
		if (getData().getEndTime() > 0) {
			return getData().getEndTime() % 60;
		} else {
			return (1440 + getData().getEndTime()) % 60;
		}
	}

	public int getEndHour() {
		if (getData().getEndTime() > 0) {
			return getData().getEndTime() / 60;
		} else {
			return (1440 + getData().getEndTime()) / 60;
		}
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public SilenceViewModel createFromParcel(Parcel in) {
			return new SilenceViewModel(in);
		}

		public SilenceViewModel[] newArray(int size) {
			return new SilenceViewModel[size];
		}
	};
	
}
