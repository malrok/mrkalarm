package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.models.Rule;

public class RulesListViewModel extends AbstractListViewModel<Rule> {

	public RulesListViewModel() {
		super();
	}

	private RulesListViewModel(Parcel in) {
		this.loadFromData(in.readParcelableArray(Alarm.class.getClassLoader()), RuleViewModel.class);
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Rule> dataLoader) {
		// nothing to do
	}

	@Override
	public RulesListViewModel copy() {
		RulesListViewModel rules = new RulesListViewModel();

		for (Rule rule : this.getDataList()) {
			RuleViewModel vm = new RuleViewModel();
			vm.loadFromData(rule.copy());
			rules.getViewModelList().add(vm);
		}

		return rules;
	}

	public RuleViewModel getRuleForEntryName(String entryName) {
		RuleViewModel foundRuleVM = null;

		for (IViewModel<Rule> ruleVM : this.getViewModelList()) {
			if (ruleVM.getData().getRegex().length() == 0) {
				// the rule will trigger on any entry
				foundRuleVM = (RuleViewModel) ruleVM;
			} else if (entryName.toLowerCase().contains(ruleVM.getData().getRegex().toLowerCase()) && foundRuleVM == null) {
				// the rule matches the entry
				foundRuleVM = (RuleViewModel) ruleVM;
			}
		}

		return foundRuleVM;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public RulesListViewModel createFromParcel(Parcel in) {
			return new RulesListViewModel(in);
		}

		public RulesListViewModel[] newArray(int size) {
			return new RulesListViewModel[size];
		}
	};
}
