package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.dataloaders.AlarmsDataLoader;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.models.Rule;
import com.mrk.smartalarm.models.Silence;

public class AgendasListViewModel extends AbstractListViewModel<Agenda> {

	public AgendasListViewModel() {
		super();
	}

	private AgendasListViewModel(Parcel in) {
		this.loadFromData(in.readParcelableArray(Agenda.class.getClassLoader()), AgendaViewModel.class);
	}

	@Override
	public void clear() {
		this.getDataList().clear();
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Agenda> dataLoader) {
		// nothing to do
	}

	public void loadFromAlarmDataLoader(AlarmsDataLoader loader, int identifier) {
		clear();

		for (Agenda agenda : loader.getAgendasForAlarm(identifier)) {
			AgendaViewModel agendaVM = new AgendaViewModel();
			agendaVM.loadFromData(agenda);

			for (Rule rule : loader.getRulesForAgenda(agenda.getIdentifier())) {
				if (rule.getAgendaIdentifier() == agenda.getIdentifier()) {
					RuleViewModel ruleVM = new RuleViewModel();
					ruleVM.loadFromData(rule);
					agendaVM.getRulesList().addVM(ruleVM);

					for (Silence silence : loader.getSilencesForRule(rule.getIdentifier())) {
						if (silence.getLinkedIdentifier() == rule.getIdentifier()) {
							SilenceViewModel silenceVM = new SilenceViewModel();
							silenceVM.loadFromData(silence);
							ruleVM.getSilencesList().addVM(silenceVM);
						}
					}
				}

			}

			this.getViewModelList().add(agendaVM);
		}
	}

	@Override
	public IListViewModel<Agenda> copy() {
		AgendasListViewModel newVM = new AgendasListViewModel();

		for (Agenda agenda : this.getDataList()) {
			AgendaViewModel vm = new AgendaViewModel();
			vm.loadFromData(agenda.copy());
			newVM.getViewModelList().add(vm);
		}

		return newVM;
	}

	public boolean hasRules() {
		boolean hasRules = false;

		for (IViewModel<Agenda> agendaVM : this.getViewModelList()) {
			hasRules |= ! ((AgendaViewModel)agendaVM).getRulesList().getViewModelList().isEmpty();
		}

		return hasRules;
	}

	public RuleViewModel getRuleById(int ruleId) {
		RuleViewModel rule = null;

		for (IViewModel<Agenda> agendaVM : this.getViewModelList()) {
			for (IViewModel<Rule> ruleVM : ((AgendaViewModel) agendaVM).getRulesList().getViewModelList()) {
				if (ruleVM.getData().getIdentifier() == ruleId) {
					rule = (RuleViewModel) ruleVM;
				}
			}
		}

		return rule;
	}

	public String summary(Context context) {
		String summary = "";

		for (IViewModel<Agenda> agendaVM : this.getViewModelList()) {
			if (!((AgendaViewModel)agendaVM).getRulesList().getViewModelList().isEmpty()) {
				if (summary.length() > 0) {
					summary += "\n";
				}
				summary += agendaVM.getData().getTitle();
			}
		}

		return summary;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		Parcelable[] list = new Parcelable[this.getViewModelList().size()];

		for (int rank = 0; rank < this.getViewModelList().size(); rank++) {
			Parcelable parcelable = this.getViewModelList().get(rank);
			list[rank] = parcelable;
		}

		parcel.writeParcelableArray(list, 0);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public AgendasListViewModel createFromParcel(Parcel in) {
			return new AgendasListViewModel(in);
		}

		public AgendasListViewModel[] newArray(int size) {
			return new AgendasListViewModel[size];
		}
	};
}
