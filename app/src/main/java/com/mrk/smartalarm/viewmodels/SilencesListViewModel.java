package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.helpers.DaysOfTheWeekHelper;
import com.mrk.smartalarm.engine.helpers.TimeHelper;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.models.Silence;

import java.util.ArrayList;
import java.util.List;

public class SilencesListViewModel extends AbstractListViewModel<Silence> {

	public SilencesListViewModel() {
		super();
	}

	private SilencesListViewModel(Parcel in) {
		this.loadFromData(in.readParcelableArray(Alarm.class.getClassLoader()), SilenceViewModel.class);
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Silence> dataLoader) {
		// nothing to do
	}

	public void loadFromData(List<Silence> dataList) {
		clear();

		for (Silence data : dataList) {
			SilenceViewModel vm = new SilenceViewModel();
			vm.loadFromData(data);
			this.getViewModelList().add(vm);
		}
	}

	@Override
	public SilencesListViewModel copy() {
		SilencesListViewModel silences = new SilencesListViewModel();

		for (Silence silence : this.getDataList()) {
			SilenceViewModel vm = new SilenceViewModel();
			vm.loadFromData(silence.copy());
			silences.getViewModelList().add(vm);
		}

		return silences;
	}

	public SilenceViewModel getSilenceById(int silenceId) {
		for (IViewModel<Silence> silenceVM : this.getViewModelList()) {
			if (silenceVM.getData().getIdentifier() == silenceId) {
				return (SilenceViewModel) silenceVM;
			}
		}
		return null;
	}

	public String summary(Context context) {
		String ret = "";

		for (int rank = 0; rank < Math.min(2, this.getViewModelList().size()); rank++) {
			SilenceViewModel vm = (SilenceViewModel) this.getViewModelList().get(rank);
			if (ret.length() > 0) {
				ret += "\n";
			}

			switch (vm.getData().getSilenceMode()) {
				case 0: // vibrate
					ret += context.getString(R.string.vibrates) + " ";
					break;
				case 1: // silent
					ret += context.getString(R.string.muted) + " ";
					break;
				case 2: // favorites
					ret += context.getString(R.string.starred) + " ";
					break;
			}

			ret += context.getString(R.string.from) + " ";
			ret += TimeHelper.getFormattedTime(context, vm.getBeginHour(), vm.getBeginMinutes());
			ret += " " + context.getString(R.string.to) + " ";
			ret += TimeHelper.getFormattedTime(context, vm.getEndHour(), vm.getEndMinutes());
		}

		return ret;
	}

	public List<SilenceViewModel> getSilencesForDay(int day) {
		List<SilenceViewModel> result = new ArrayList<>();

		int dayAfter = day + 1;
		if (dayAfter > 7) {
			dayAfter = 1;
		}

		for (IViewModel<Silence> silenceVM : this.getViewModelList()) {
			if (DaysOfTheWeekHelper.getWeekArray(silenceVM.getData())[day]
					|| (DaysOfTheWeekHelper.getWeekArray(silenceVM.getData())[dayAfter] && silenceVM.getData().getBeginTime() < 0)) {
				result.add((SilenceViewModel) silenceVM);
			}
		}

		return result;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public SilencesListViewModel createFromParcel(Parcel in) {
			return new SilencesListViewModel(in);
		}

		public SilencesListViewModel[] newArray(int size) {
			return new SilencesListViewModel[size];
		}
	};
}
