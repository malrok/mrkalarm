package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Rule;
import com.mrk.smartalarm.models.Silence;

public class RuleViewModel extends AbstractViewModel<Rule> {

	private SilencesListViewModel silencesList;

	public RuleViewModel() {
		super();
		this.data = new Rule();

		this.getData().setIdentifier(IdentifierHelper.getInstance().getIdentifierForClass(Rule.class));

		silencesList = new SilencesListViewModel();
	}

	private RuleViewModel(Parcel in) {
		this.loadFromData((Rule) in.readParcelable(Rule.class.getClassLoader()));
	}

	public SilencesListViewModel getSilencesList() {
		return silencesList;
	}

	public String getModeDescription(Context context) {
		int string = 0;

		switch (this.getData().getMode()) {
			case NO_RING:
				string = R.string.wont_ring;
				break;
			case RULE_TIME:
				string = R.string.rule_time;
				break;
			case ENTRY_TIME:
				string = R.string.entry_time;
				break;
		}

		return context.getString(string);
	}

	public SilenceViewModel getSilenceById(int silenceId) {
		SilenceViewModel silence = null;

		for (IViewModel<Silence> silenceVM : this.silencesList.getViewModelList()) {
			if (silenceVM.getData().getIdentifier() == silenceId) {
				silence = (SilenceViewModel) silenceVM;
			}
		}

		return silence;
	}

	/**
	 * returns the silences summary.
	 * Used for binding
	 * @param context android context
	 * @return the summary to display
	 */
	public String silencesSummary(Context context) {
		if (this.getSilencesList().getViewModelList().isEmpty()) {
			return context.getString(R.string.no_silences_set);
		} else {

			return this.getSilencesList().summary(context);
		}
	}

	@Override
	public RuleViewModel copy() {
		RuleViewModel newVM = new RuleViewModel();

		newVM.loadFromData(this.data);

		return newVM;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public RuleViewModel createFromParcel(Parcel in) {
			return new RuleViewModel(in);
		}

		public RuleViewModel[] newArray(int size) {
			return new RuleViewModel[size];
		}
	};
}
