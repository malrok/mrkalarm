package com.mrk.smartalarm.viewmodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.smartalarm.models.Music;

public class MusicViewModel extends AbstractViewModel<Music> {

	public MusicViewModel() {
		super();
		this.data = new Music();
		this.getData().setIdentifier(IdentifierHelper.getInstance().getIdentifierForClass(Music.class));
	}

	private MusicViewModel(Parcel in) {
		this.loadFromData((Music) in.readParcelable(Music.class.getClassLoader()));
	}

	@Override
	public MusicViewModel copy() {
		MusicViewModel newVM = new MusicViewModel();

		newVM.loadFromData(this.data);

		return newVM;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public MusicViewModel createFromParcel(Parcel in) {
			return new MusicViewModel(in);
		}

		public MusicViewModel[] newArray(int size) {
			return new MusicViewModel[size];
		}
	};

}
