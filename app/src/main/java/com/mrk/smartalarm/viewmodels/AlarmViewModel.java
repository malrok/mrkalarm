package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.helpers.DaysOfTheWeekHelper;
import com.mrk.smartalarm.engine.helpers.TimeHelper;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.models.Alarm;

import java.util.List;

public class AlarmViewModel extends AbstractViewModel<Alarm> {

	/* linked view models */
	private MusicListViewModel musicList;
	private AgendasListViewModel agendasList;
	private SilencesListViewModel silencesList;

	public AlarmViewModel() {
		super();
		this.data = new Alarm();

		this.getData().setIdentifier(IdentifierHelper.getInstance().getIdentifierForClass(Alarm.class));

		musicList = new MusicListViewModel();
		agendasList = new AgendasListViewModel();
		silencesList = new SilencesListViewModel();
	}

	private AlarmViewModel(Parcel in) {
		this.loadFromData((Alarm) in.readParcelable(Alarm.class.getClassLoader()));
	}

	@Override
	public AlarmViewModel copy() {
		AlarmViewModel newVM = new AlarmViewModel();

		newVM.loadFromData(this.data);

		return newVM;
	}

	public MusicListViewModel getMusicList() {
		return musicList;
	}

	public void setMusicList(MusicListViewModel musicList) {
		this.musicList = musicList;
	}

	public AgendasListViewModel getAgendasList() {
		return agendasList;
	}

	public void setAgendasList(AgendasListViewModel agendasList) {
		this.agendasList = agendasList;
	}

	public SilencesListViewModel getSilencesList() {
		return silencesList;
	}

	public void setSilencesList(SilencesListViewModel silencesList) {
		this.silencesList = silencesList;
	}

	/**
	 * Returns the time of the alarm as a string.
	 * Used for binding
	 * @param context android context
	 * @return the time as a string
	 */
	public String getTime(Context context) {
		String ret;

		if ((this.getData().isMonday() || this.getData().isTuesday() || this.getData().isWednesday()
				|| this.getData().isThursday() || this.getData().isFriday() || this.getData().isSaturday() || this.getData().isSunday())
				|| this.isOneShot()) {
			ret = TimeHelper.getFormattedTime(context, this.getData().getTime().getHours(), this.getData().getTime().getMinutes());
		} else {
			ret = context.getResources().getString(R.string.no_time_defined);
		}

		return ret;
	}

	/**
	 * returns the silences summary.
	 * Used for binding
	 * @param context android context
	 * @return the summary to display
	 */
	public String silencesSummary(Context context) {
		if (this.getSilencesList().getViewModelList().isEmpty()) {
			return context.getString(R.string.no_silences_set);
		} else {

			return this.getSilencesList().summary(context);
		}
	}

	/**
	 * returns the agenda summary.
	 * Used for binding
	 * @param context android context
	 * @return the summary to display
	 */
	public String agendaSummary(Context context) {
		if (!this.getAgendasList().hasRules()) {
			return context.getString(R.string.no_rules_set);
		} else {
			return this.getAgendasList().summary(context);
		}
	}

	/**
	 * Returns the alarm ring summary.
	 * Used for binding
	 * @param context android context
	 * @return the summary to display
	 */
	public String ringSummary(Context context) {
		return "ring";
	}

	public boolean isOneShot() {
		return !this.getData().isMonday() && !this.getData().isTuesday()
				&& !this.getData().isWednesday() && !this.getData().isThursday()
				&& !this.getData().isFriday() && !this.getData().isSaturday() && !this.getData().isSunday()
				&& !this.getAgendasList().hasRules();
	}

	public boolean isAgendaUsed(String name) {
		boolean isUsed = false;

		for (IViewModel<Agenda> vm : getAgendasList().getViewModelList()) {
			if (vm.getData().getTitle().equals(name)) {
				isUsed = true;
			}
		}

		return isUsed;
	}

	public boolean hasAlarmForDay(int day) {
		return DaysOfTheWeekHelper.getWeekArray(this.getData())[day];
	}

	public List<SilenceViewModel> getSilencesForDay(int day) {
		return this.silencesList.getSilencesForDay(day);
	}

	public boolean hasSilencesForDay(int day) {
		return this.silencesList.getSilencesForDay(day).size() > 0;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public AlarmViewModel createFromParcel(Parcel in) {
			return new AlarmViewModel(in);
		}

		public AlarmViewModel[] newArray(int size) {
			return new AlarmViewModel[size];
		}
	};
}
