package com.mrk.smartalarm.viewmodels.factories;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;

public class AgendaViewModelFactory {

	private int alarmIdentifier;
	private int lastPriority;

	public AgendaViewModelFactory(int alarmIdentifier) {
		this.alarmIdentifier = alarmIdentifier;
	}

	public AgendaViewModel newInstance(String title) {
		AgendaViewModel vm = new AgendaViewModel();

		Agenda data = new Agenda();

		data.setIdentifier(IdentifierHelper.getInstance().getIdentifierForClass(Agenda.class));
		data.setAlarmIdentifier(this.alarmIdentifier);
		data.setTitle(title);
		data.setPriority(this.lastPriority++);

		vm.loadFromData(data);

		return vm;
	}

}
