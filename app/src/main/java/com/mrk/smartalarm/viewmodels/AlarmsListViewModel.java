package com.mrk.smartalarm.viewmodels;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.dataloaders.AlarmsDataLoader;
import com.mrk.smartalarm.models.Alarm;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AlarmsListViewModel extends AbstractListViewModel<Alarm> {

	public AlarmsListViewModel() {
		super();
	}

	private AlarmsListViewModel(Parcel in) {
		this.loadFromData(in.readParcelableArray(Alarm.class.getClassLoader()), AlarmViewModel.class);
	}

	@Override
	public void clear() {
		this.getDataList().clear();
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<Alarm> dataLoader) {
		clear();

		AlarmsDataLoader loader = (AlarmsDataLoader) dataLoader;

		List<Alarm> dataList = loader.getData(context);

		for (Alarm data : dataList) {
			AlarmViewModel vm = new AlarmViewModel();
			vm.loadFromData(data);

			MusicListViewModel musicList = new MusicListViewModel();
			musicList.loadFromData(loader.getMusicForAlarm(data.getIdentifier()));
			vm.setMusicList(musicList);

			AgendasListViewModel agendasList = new AgendasListViewModel();
			agendasList.loadFromAlarmDataLoader(loader, data.getIdentifier());
			vm.setAgendasList(agendasList);

			SilencesListViewModel silencesList = new SilencesListViewModel();
			silencesList.loadFromData(loader.getSilencesForAlarm(data.getIdentifier()));
			vm.setSilencesList(silencesList);

			this.getViewModelList().add(vm);
		}

		if (getListener() != null) getListener().finishedLoading(this);
	}

	@Override
	public IListViewModel<Alarm> copy() {
		AlarmsListViewModel newVM = new AlarmsListViewModel();

		for (Alarm alarm : this.getDataList()) {
			AlarmViewModel vm = new AlarmViewModel();
			vm.loadFromData(alarm.copy());
			newVM.getViewModelList().add(vm);
		}

		return newVM;
	}

	/**
	 * Returns a description of the next alarm.
	 * Used for binding
	 * @param context android context
	 * @return the next alarm as string
	 */
	public String getNextAlarmText(Context context) {
		Date date = null;

		AlarmViewModel nextAlarm = getNextAlarm();

		if (nextAlarm != null) {
			date = new Date(nextAlarm.getData().getNextAlarm());
		}

		String ret = context.getString(R.string.no_next_alarm);

		if (date != null) {
			ret = context.getString(R.string.next_alarm) + " " +
					new SimpleDateFormat("dd/MM HH:mm", Locale.getDefault()).format(date.getTime());
		}

		return ret;
	}

	public AlarmViewModel getNextAlarm() {
		AlarmViewModel nextAlarm = null;

		Date date = null;

		for (IViewModel<Alarm> alarmVM : this.getViewModelList()) {
			if (alarmVM.getData().isActivated() && alarmVM.getData().getNextAlarm() != null) {
				Date alarmDate = new Date(alarmVM.getData().getNextAlarm());
				if (date == null) {
					date = new Date(alarmDate.getTime());
					nextAlarm = (AlarmViewModel) alarmVM;
				} else if (date.after(alarmDate)) {
					date.setTime(alarmDate.getTime());
					nextAlarm = (AlarmViewModel) alarmVM;
				}
			}
		}

		return nextAlarm;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		Parcelable[] list = new Parcelable[this.getViewModelList().size()];

		for (int rank = 0; rank < this.getViewModelList().size(); rank++) {
			Parcelable parcelable = this.getViewModelList().get(rank);
			list[rank] = parcelable;
		}

		parcel.writeParcelableArray(list, 0);
	}

	public AlarmViewModel getViewModel(int identifier) {
		AlarmViewModel model = null;

		for (IViewModel<Alarm> vm : this.getViewModelList()) {
			if (vm.getData().getIdentifier() == identifier) {
				model = (AlarmViewModel) vm;
			}
		}

		return model;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public AlarmsListViewModel createFromParcel(Parcel in) {
			return new AlarmsListViewModel(in);
		}

		public AlarmsListViewModel[] newArray(int size) {
			return new AlarmsListViewModel[size];
		}
	};
}
