package com.mrk.smartalarm.ui.fragments;

import android.view.View;

import com.mrk.mrkframework.ui.AbstractMRKRecyclerViewFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Music;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.adapters.AlarmMusicListAdapter;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;

public class AlarmMusicListFragment extends AbstractMRKRecyclerViewFragment<Music> implements View.OnClickListener {

	@Override
	public void onStart() {
		super.onStart();

		this.getRootView().findViewById(R.id.add_music).setOnClickListener(this);
	}

	@Override
	public int getLayout() {
		return R.layout.alarmmusiclist_fragment;
	}

	@Override
	public int getItemLayout() {
		return R.layout.alarmmusiclist_item;
	}

	@Override
	public Class<?> getAdapterClass() {
		return AlarmMusicListAdapter.class;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add_music) {
			((AlarmSetScreen) this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMSMUSIC_ADD.toString());
		}
	}
}
