package com.mrk.smartalarm.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.getpebble.android.kit.PebbleKit;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.alarm.RingPlayer;
import com.mrk.smartalarm.engine.alarm.VolumeSetter;
import com.mrk.smartalarm.engine.broadcasts.BroadcastsHelper;
import com.mrk.smartalarm.engine.events.RingStartEvent;
import com.mrk.smartalarm.engine.events.RingStopEvent;
import com.mrk.smartalarm.engine.helpers.ServiceHelper;
import com.mrk.smartalarm.engine.pebble.PebbleCommunicationService;
import com.mrk.smartalarm.engine.pebble.PebbleConstants;
import com.mrk.smartalarm.ui.views.Slider;
import com.mrk.smartalarm.ui.views.interfaces.SliderListener;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;

import de.greenrobot.event.EventBus;

public class AlarmRingScreen extends Activity {

	private AlarmViewModel alarmViewModel;
	private RingPlayer ringPlayer;
	private VolumeSetter volumeSetter;

	private static MediaPlayer player;

	private PowerManager.WakeLock wakeLock;

	private BroadcastReceiver playerCompletionReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			stop(false);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent().hasExtra(BroadcastsHelper.ALARM_PARCELABLE)) {
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

			this.alarmViewModel = getIntent().getParcelableExtra(BroadcastsHelper.ALARM_PARCELABLE);

			// used to show when phone is off and locked
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
					| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
					| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			setContentView(R.layout.alarm_ring_screen);

			((TextView)findViewById(R.id.name)).setText(this.alarmViewModel.getData().getName());

			findViewById(R.id.sleep).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					stop(true);
				}
			});

			((Slider) findViewById(R.id.slider)).setSliderListener(new SliderListener() {
				@Override
				public void onProgress(int progress) {
					// nothing to do
				}

				@Override
				public void onMaxReached() {
					stop(false);
				}
			});

			if (!PebbleKit.isWatchConnected(getApplicationContext())) {
				ringPlayer = new RingPlayer();
				volumeSetter = new VolumeSetter(this);

				if (player == null || !player.isPlaying()) {
					// TODO: make better after music selection is done
					player = ringPlayer.startPlayer(this, this.alarmViewModel.getData().getAlarmType(), "", false);
					volumeSetter.increaseVolume(this.alarmViewModel.getData().isVolumeProgression() ? 15 : 0, this.alarmViewModel.getData().getAlarmMaxVolume());
				}
			} else {
				if (!ServiceHelper.isServiceRunning(this, PebbleCommunicationService.class)) {
					ServiceHelper.startService(this, PebbleCommunicationService.class);
				}
				EventBus.getDefault().post(new RingStartEvent(this.alarmViewModel));
			}

		} else {
			finish();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		wakeLock.acquire();
		IntentFilter filter = new IntentFilter();
		filter.addAction(RingPlayer.MUSIC_COMPLETED);
		LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(playerCompletionReceiver, filter);
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		EventBus.getDefault().unregister(this);
		LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(playerCompletionReceiver);
		wakeLock.release();
		super.onStop();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putParcelable("alarmViewModel", this.alarmViewModel);
		if (this.volumeSetter != null) {
			this.volumeSetter.onSaveInstanceState(outState);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		this.alarmViewModel = savedInstanceState.getParcelable("alarmViewModel");
		if (this.volumeSetter != null) {
			this.volumeSetter.onRestoreInstanceState(savedInstanceState);
		}
		super.onRestoreInstanceState(savedInstanceState);
	}

	// EventBus callback
	public void onEvent(RingStopEvent event) {
		stop(event.isRepeat());
	}

	private void stop(boolean repeat) {
		if (!PebbleKit.isWatchConnected(getApplicationContext())) {
			ringPlayer.stop(player);
			player = null;
		} else {
			PebbleKit.closeAppOnPebble(getApplicationContext(), PebbleConstants.PEBBLE_APP_UUID);
		}

		if (repeat) {
			BroadcastsHelper.sendAlarmRepeatBroadcast(this, this.alarmViewModel);
		} else {
			BroadcastsHelper.sendNextAlarmBroadcast(this);
		}

		finish();
	}
}
