package com.mrk.smartalarm.ui.formatters;

import com.appyvet.rangebar.IRangeBarFormatter;

public class SilenceBarFormatter implements IRangeBarFormatter {

	@Override
	public String format(String value) {
		String res;
		int minutes;
		int time = Integer.valueOf(value);

		if (time < 0) {
			time += 96;
		} else if (time > 96) {
			time -= 96;
		}

		minutes = (time % 4) * 15;
		res = time / 4 + "h" + (minutes != 0 ? String.valueOf(minutes) : "");

		return res;
	}
}
