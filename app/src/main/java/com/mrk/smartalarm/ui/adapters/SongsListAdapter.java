package com.mrk.smartalarm.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Song;

import java.util.List;

public class SongsListAdapter extends BaseAdapter {

	private Context context;

	private List<Song> songs;

	public SongsListAdapter(Context context, List<Song> songs) {
		super();
		this.context = context;
		this.songs = songs;
	}

	@Override
	public int getCount() {
		return this.songs.size();
	}

	@Override
	public Song getItem(int position) {
		return this.songs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);

			rowView = inflater.inflate(R.layout.songslist_item, null);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.textView = (TextView) rowView.findViewById(R.id.name);
			viewHolder.checkBox = (CheckBox) rowView.findViewById(R.id.selected);

			rowView.setTag(viewHolder);
		}

		ViewHolder holder = (ViewHolder) rowView.getTag();

		holder.textView.setText(songs.get(position).getTitle());
		holder.checkBox.setChecked(songs.get(position).selected);
		holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				songs.get(position).setSelected(isChecked);
			}
		});

		return rowView;
	}

	private static class ViewHolder {
		public CheckBox checkBox;
		public TextView textView;
	}

}
