package com.mrk.smartalarm.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.appyvet.rangebar.RangeBar;
import com.mrk.mrkframework.databinding.interfaces.ChangeListener;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.ui.formatters.SilenceBarFormatter;
import com.mrk.smartalarm.viewmodels.SilenceViewModel;

public class SilenceDialog extends Dialog {

	private SilenceViewModel vm;

	private WeekDays days;
	private RangeBar silencePeriod;
	private RadioGroup silenceMode;

	private boolean isValidated = false;

	public SilenceDialog(Context context) {
		this(context, 0);
	}

	public SilenceDialog(Context context, int themeResId) {
		super(context, themeResId);

		setTitle(R.string.set_silence);

		setContentView(R.layout.silence_dialog);

		days = (WeekDays) findViewById(R.id.days);
		silencePeriod = (RangeBar) findViewById(R.id.silence_period);
		silenceMode = (RadioGroup) findViewById(R.id.silence_mode);

		Button validate = (Button) findViewById(R.id.validate);
		Button cancel = (Button) findViewById(R.id.cancel);

		validate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SilenceDialog.this.isValidated = true;
				SilenceDialog.this.dismiss();
			}
		});

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SilenceDialog.this.dismiss();
			}
		});

		vm = new SilenceViewModel();
	}

	@Override
	protected void onStart() {
		super.onStart();

		days.addChangeListener(new ChangeListener() {
			@Override
			public void onValueChanged(Object curr, Object prev) {
				vm.getData().setDays(days.getSelection());
			}
		});

		silencePeriod.setClickable(false);
		silencePeriod.setTickStart(-16);
		silencePeriod.setTickEnd(144);
		silencePeriod.setDrawTicks(false);
		silencePeriod.setTemporaryPins(false);
		silencePeriod.setRangePinsByValue(vm.getData().getBeginTime() / 15, vm.getData().getEndTime() / 15);
		silencePeriod.setFormatter(new SilenceBarFormatter());

		silencePeriod.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
			@Override
			public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
				vm.getData().setBeginTime((short) (Integer.valueOf(leftPinValue) * 15));
				vm.getData().setEndTime((short) (Integer.valueOf(rightPinValue) * 15));
			}
		});

		switch (vm.getData().getSilenceMode()) {
			case 0:
				silenceMode.check(R.id.vibration);
				break;
			case 1:
				silenceMode.check(R.id.silence);
				break;
			case 2:
				silenceMode.check(R.id.favorites);
				break;
		}

		silenceMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				int mode = 0;

				switch (checkedId) {
					case R.id.vibration:
						mode = 0;
						break;
					case R.id.silence:
						mode = 1;
						break;
					case R.id.favorites:
						mode = 2;
						break;
				}

				vm.getData().setSilenceMode(mode);
			}
		});
	}

	public SilenceDialog setViewModel(SilenceViewModel vm) {
		this.vm = vm;

		days.setSelection(vm.getData().getDays());
		silencePeriod.setRangePinsByValue(vm.getData().getBeginTime() / 15, vm.getData().getEndTime() / 15);

		return this;
	}

	public SilenceViewModel getViewModel() {
		return this.vm;
	}

	public boolean isValidated() {
		return isValidated;
	}
}
