package com.mrk.smartalarm.ui.adapters;

import android.view.View;

import com.mrk.mrkframework.ui.adapter.AbstractRecyclerViewFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Agenda;

import java.util.Collections;

public class AlarmAgendaListAdapter extends AbstractRecyclerViewFragmentAdapter<Agenda> {

	private IListViewModel<Agenda> vmList;

	public AlarmAgendaListAdapter(IListViewModel<Agenda> vmList) {
		super(vmList);
		this.vmList = vmList;
	}

	@Override
	public int getItemLayout(int viewType) {
		return R.layout.alarmagendalist_item;
	}

	@Override
	public void onClick(View v) {
		clearSelections();
		super.onClick(v);
	}

	public void swapPositions(int from, int to) {
		Collections.swap(this.vmList.getViewModelList(), from, to);
	}
}
