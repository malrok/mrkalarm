package com.mrk.smartalarm.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.AdapterView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mrk.mrkframework.ui.AbstractMRKRecyclerViewFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.helpers.CalendarHelper;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.adapters.AlarmAgendaListAdapter;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;
import com.mrk.smartalarm.viewmodels.factories.AgendaViewModelFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class AlarmAgendaListFragment extends AbstractMRKRecyclerViewFragment<Agenda> implements View.OnClickListener, PermissionListener {

	private Map<String, Boolean> agendaSelection;
	private AgendaViewModelFactory factory;
	private ItemTouchHelper touchHelper;

	@Override
	public void onStart() {
		super.onStart();

		this.getRootView().findViewById(R.id.add_agenda).setOnClickListener(this);

		fillCalendarSelection();

		touchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {

			@Override
			public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
				int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
				int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
				return makeMovementFlags(dragFlags, swipeFlags);
			}

			@Override
			public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
				((AlarmAgendaListAdapter)getRecycler().getAdapter()).swapPositions(viewHolder.getAdapterPosition(), target.getAdapterPosition());
				getRecycler().getAdapter().notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());

				return true;
			}

			@Override
			public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
				AgendaViewModel vm = (AgendaViewModel) AlarmAgendaListFragment.this.getListViewModel().getViewModelList().get(viewHolder.getAdapterPosition());
				AlarmAgendaListFragment.this.getListViewModel().removeVM(vm);
				AlarmAgendaListFragment.this.getRecycler().getAdapter().notifyItemRemoved(viewHolder.getAdapterPosition());
			}
		});

		touchHelper.attachToRecyclerView(getRecycler());
	}

	@Override
	public void onStop() {
		touchHelper.attachToRecyclerView(null);
		super.onStop();
	}

	private void fillCalendarSelection() {
		agendaSelection = new HashMap<>();

		new TedPermission(this.getActivity())
				.setPermissionListener(this)
				.setDeniedMessage(getString(R.string.mandatory_calendar_permission))
				.setPermissions(Manifest.permission.READ_CALENDAR)
				.check();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		this.getListViewModel().setSelectedItem(position);
		((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMAGENDA_SELECT.toString());
	}

	@Override
	public int getLayout() {
		return R.layout.alarmagendalist_fragment;
	}

	@Override
	public int getItemLayout() {
		return R.layout.alarmagendalist_item;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}

	@Override
	public Class<AlarmAgendaListAdapter> getAdapterClass() {
		return AlarmAgendaListAdapter.class;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add_agenda) {
			final boolean[] temp = Arrays.copyOf(getSelectedCalendars(), agendaSelection.size());

			AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

			builder.setTitle(R.string.select_days)
					.setMultiChoiceItems(
							getCalendarsList(),
							getSelectedCalendars(),
							new DialogInterface.OnMultiChoiceClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which, boolean isChecked) {
									temp[which] = isChecked;
								}
							})
					.setPositiveButton(R.string.button_valid, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							for (int i=0; i<agendaSelection.size(); i++) {
								String key = agendaSelection.keySet().toArray(new String[agendaSelection.keySet().size()])[i];
								if (!agendaSelection.get(key) && temp[i]) {
									AlarmAgendaListFragment.this.getListViewModel().addVM(getFactory().newInstance(key));
								}
								agendaSelection.put(key, temp[i]);
							}
							dialog.dismiss();
						}
					})
					.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});


			AlertDialog dialog = builder.create();

			dialog.show();
		}
	}

	public int getSelectedItemsNumber() {
		return ((AlarmAgendaListAdapter)this.getRecycler().getAdapter()).getSelectedItemCount();
	}

	public void toggleSelection(int rank) {
		((AlarmAgendaListAdapter) getRecycler().getAdapter()).toggleSelection(rank);
	}

	private String[] getCalendarsList() {
		return agendaSelection.keySet().toArray(new String[agendaSelection.keySet().size()]);
	}

	private boolean[] getSelectedCalendars() {
		boolean[] selection = new boolean[agendaSelection.values().size()];

		for (int i = 0; i < agendaSelection.values().size(); i++) {
			selection[i] = agendaSelection.values().toArray(new Boolean[agendaSelection.values().size()])[i];
		}

		return selection;
	}

	private AgendaViewModelFactory getFactory() {
		if (factory == null) {
			factory = new AgendaViewModelFactory(((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem().getData().getIdentifier());
		}
		return factory;
	}

	@Override
	public void onPermissionGranted() {
		for (String agendaTitle : CalendarHelper.getCalendarsList(this.getActivity())) {
			boolean found = false;

			for (Agenda agenda : this.getDataList()) {
				if (agendaTitle.equals(agenda.getTitle())) {
					found = true;
				}
			}

			agendaSelection.put(agendaTitle, found);
		}
	}

	@Override
	public void onPermissionDenied(ArrayList<String> arrayList) {
		Snackbar.make(this.getRootView(), R.string.mandatory_calendar_permission, Snackbar.LENGTH_SHORT).show();
	}
}
