package com.mrk.smartalarm.ui.fragments;

import android.view.View;

import com.mrk.mrkframework.ui.AbstractMRKFragment;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;

public class AlarmMasterFragment extends AbstractMRKFragment implements View.OnClickListener {

	@Override
	public void onStart() {
		super.onStart();

		this.getRootView().findViewById(R.id.detail).setOnClickListener(this);

		this.getRootView().findViewById(R.id.silences).setOnClickListener(this);

		this.getRootView().findViewById(R.id.agenda).setOnClickListener(this);

		this.getRootView().findViewById(R.id.ring).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.detail:
				((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMMASTER_DETAIL.toString());
				break;
			case R.id.silences:
				((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMMASTER_SILENCES.toString());
				break;
			case R.id.agenda:
				((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMMASTER_AGENDA.toString());
				break;
			case R.id.ring:
				((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMMASTER_RING.toString());
				break;
		}
	}

	@Override
	public int getLayout() {
		return R.layout.alarmmaster_fragment;
	}

	@Override
	public IViewModel getViewModel() {
		return ((AlarmSetScreen)getActivity()).getAlarmsViewModel().getSelectedItem();
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}
}
