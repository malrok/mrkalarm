package com.mrk.smartalarm.ui.delegates;

import com.mrk.mrkframework.ui.fragmentdelegate.AbstractDelegateActions;

public class HostScreenDelegateAction extends AbstractDelegateActions {

    public enum Actions {
        /** activity */
        ACTIVITY_RESUME,

        /** ALARMS LIST FRAGMENT*/
        ALARMLIST_PUSH,
        ALARMSLIST_SELECT,
        ALARMSLIST_ADD,

        /** ALARM MASTER DETAIL */
        ALARMMASTER_DETAIL,

		/** ALARM SILENCES */
        ALARMMASTER_SILENCES,

        /** ALARM AGENDA */
        ALARMMASTER_AGENDA,
        ALARMAGENDA_SELECT,

        /** ALARM_RULE */
        ALARMSRULE_SELECT,
        ALARMSRULE_ADD,
        ALARMSRULE_SILENCES,

        /** ALARM_RING */
        ALARMMASTER_RING,
        ALARMSMUSIC_ADD,

        /** PREFERENCES */
        PREFERENCES_NAVIGATE,
    }

}
