package com.mrk.smartalarm.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;

import com.appyvet.rangebar.RangeBar;
import com.mrk.mrkframework.databinding.interfaces.BindableView;
import com.mrk.mrkframework.databinding.interfaces.ChangeListener;
import com.mrk.smartalarm.R;

import java.util.ArrayList;
import java.util.List;

public class BindableRangeBar extends RangeBar implements BindableView, RangeBar.OnRangeBarChangeListener {

	private List<ChangeListener> changedListeners;

	private int previousValues;

	private int minValue;
	private int maxValue;

	public BindableRangeBar(Context context) {
		this(context, null);
	}

	public BindableRangeBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public BindableRangeBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		changedListeners = new ArrayList<>();

		init(context, attrs);

		this.setRangeBarEnabled(false);
		this.setTemporaryPins(false);
		this.setOnRangeBarChangeListener(this);
	}

	private void init(Context context, AttributeSet attrs) {
		if (attrs != null) {
			TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BindableRangeBar, 0, 0);

			minValue = typedArray.getInt(R.styleable.BindableRangeBar_lowerValue, 0);
			maxValue = typedArray.getInt(R.styleable.BindableRangeBar_higherValue, 10);

			this.setTickStart(minValue);
			this.setTickEnd(maxValue);

			typedArray.recycle();
		}
	}

	@Override
	public void addChangeListener(ChangeListener changeListener) {
		changedListeners.add(changeListener);
	}

	@Override
	public void setValue(Object value) {
		if (value instanceof Integer) {
			int intValue = (int) value;

			if (intValue < minValue) {
				intValue = minValue;
			}
			if (intValue > maxValue) {
				intValue = maxValue;
			}

			setRangePinsByValue(minValue, intValue);
		} else {
			Log.e(BindableRangeBar.class.getSimpleName(), "set value should be an int object");
		}
	}

	@Override
	public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
		for (ChangeListener listener : this.changedListeners) {
			listener.onValueChanged(previousValues, Integer.parseInt(rightPinValue));
			previousValues = Integer.parseInt(rightPinValue);
		}
	}
}
