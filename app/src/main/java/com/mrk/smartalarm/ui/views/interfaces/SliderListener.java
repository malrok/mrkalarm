package com.mrk.smartalarm.ui.views.interfaces;

public interface SliderListener {

	void onProgress(int progress);

	void onMaxReached();

}
