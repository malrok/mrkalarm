package com.mrk.smartalarm.ui.fragments;

import com.mrk.mrkframework.ui.AbstractMRKFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Alarm;

public class AlarmRingFragment extends AbstractMRKFragment<Alarm> {

	@Override
	public int getLayout() {
		return R.layout.alarmring_fragment;
	}

}
