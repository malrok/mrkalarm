package com.mrk.smartalarm.ui.adapters;

import com.mrk.mrkframework.ui.adapter.AbstractRecyclerViewFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Rule;

public class AlarmRulesListAdapter extends AbstractRecyclerViewFragmentAdapter<Rule> {

	public AlarmRulesListAdapter(IListViewModel<Rule> vmList) {
		super(vmList);
	}

	@Override
	public int getItemLayout(int viewType) {
		return R.layout.alarmruleslist_item;
	}

}
