package com.mrk.smartalarm.ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionButton;
import com.mrk.mrkframework.ui.AbstractMRKListFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Silence;
import com.mrk.smartalarm.ui.views.SilenceDialog;
import com.mrk.smartalarm.ui.views.SilencePeriods;
import com.mrk.smartalarm.ui.views.interfaces.SilencePeriodsListener;
import com.mrk.smartalarm.viewmodels.SilenceViewModel;
import com.mrk.smartalarm.viewmodels.SilencesListViewModel;

public class SilencesFragment extends AbstractMRKListFragment<Silence> implements View.OnClickListener, SilencePeriodsListener {

	public static final int ALARM_SILENCE = 0;
	public static final int RULE_SILENCE = 1;

	private SilencePeriods silencePeriods;
	private int mode = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);

		if (savedInstanceState != null && savedInstanceState.get("mode") != null) {
			this.mode = savedInstanceState.getInt("mode");
		}

		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		silencePeriods = (SilencePeriods) this.getRootView().findViewById(R.id.silences);
		silencePeriods.setSilences((SilencesListViewModel) getListViewModel());
		silencePeriods.registerListener(this);

		FloatingActionButton addSilence = (FloatingActionButton) this.getRootView().findViewById(R.id.add_silence);
		addSilence.setOnClickListener(this);
	}

	@Override
	public void onStop() {
		silencePeriods.unregisterListener(this);

		super.onStop();
	}

	@Override
	public int getLayout() {
		return R.layout.alarmsilences_fragment;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add_silence) {
			SilenceDialog dialog = new SilenceDialog(this.getActivity());
			dialog.setViewModel(new SilenceViewModel());
			dialog.show();
			dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if (((SilenceDialog)dialog).isValidated()) {
						SilenceViewModel vm = ((SilenceDialog) dialog).getViewModel();
						getListViewModel().addVM(vm);
						silencePeriods.addSilence(vm);
					}
				}
			});
		}
	}

	@Override
	public void remove(SilenceViewModel item) {
		getListViewModel().removeVM(item);
		silencePeriods.setSilences((SilencesListViewModel) getListViewModel());
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("mode", mode);
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}
}