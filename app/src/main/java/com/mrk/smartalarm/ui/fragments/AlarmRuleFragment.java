package com.mrk.smartalarm.ui.fragments;

import android.view.View;

import com.mrk.mrkframework.ui.AbstractMRKFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Rule;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;

public class AlarmRuleFragment extends AbstractMRKFragment<Rule> implements View.OnClickListener {

	@Override
	public void onStart() {
		super.onStart();

		View silences = this.getRootView().findViewById(R.id.silences);
		silences.setOnClickListener(this);
	}

	@Override
	public int getLayout() {
		return R.layout.alarmrule_fragment;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}

	@Override
	public void onClick(View v) {
		((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMSRULE_SILENCES.toString());
	}
}
