package com.mrk.smartalarm.ui.fragments;

import android.app.FragmentTransaction;

import com.mrk.mrkframework.ui.AbstractMRKFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;

public class AlarmRingMasterFragment extends AbstractMRKFragment<Alarm> { //implements AdapterLifeCycleInterface {

	private AlarmRingFragment ringFragment;
	private AlarmMusicListFragment musicListFragment;

	@Override
	public void onStart() {
		super.onStart();

		setFragments();
	}

	@Override
	public void onStop() {
//		musicListFragment.setAdapterCallback(null);

		super.onStop();
	}

	private void setFragments() {
		FragmentTransaction ft = this.getActivity().getFragmentManager().beginTransaction();

		// ring details
		ringFragment = new AlarmRingFragment();
		ringFragment.setViewModel(this.getViewModel());
		ft.replace(R.id.ringDetail, ringFragment);

		// music list
		musicListFragment = new AlarmMusicListFragment();
		musicListFragment.setListViewModel(((AlarmViewModel) this.getViewModel()).getMusicList());
//		musicListFragment.setAdapterCallback(this);
		ft.replace(R.id.musicList, musicListFragment);

		ft.commit();
	}

	@Override
	public int getLayout() {
		return R.layout.alarmringmaster_fragment;
	}

//	@Override
//	public void onAdapterSet(Fragment fragment) {
//
//	}
}
