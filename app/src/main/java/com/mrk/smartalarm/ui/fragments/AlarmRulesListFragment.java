package com.mrk.smartalarm.ui.fragments;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.mrk.mrkframework.ui.AbstractMRKRecyclerViewFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Rule;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.adapters.AlarmRulesListAdapter;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.RuleViewModel;

public class AlarmRulesListFragment extends AbstractMRKRecyclerViewFragment<Rule> implements View.OnClickListener {

	private ItemTouchHelper touchHelper;

	@Override
	public void onStart() {
		super.onStart();

		this.getRootView().findViewById(R.id.add_rule).setOnClickListener(this);

		touchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {

			@Override
			public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
				return makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE, ItemTouchHelper.START | ItemTouchHelper.END);
			}

			@Override
			public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
				return false;
			}

			@Override
			public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
				RuleViewModel vm = (RuleViewModel) AlarmRulesListFragment.this.getListViewModel().getViewModelList().get(viewHolder.getAdapterPosition());
				AlarmRulesListFragment.this.getListViewModel().removeVM(vm);
				AlarmRulesListFragment.this.getRecycler().getAdapter().notifyItemRemoved(viewHolder.getAdapterPosition());
			}
		});

		touchHelper.attachToRecyclerView(getRecycler());

	}

	@Override
	public void onStop() {
		touchHelper.attachToRecyclerView(null);

		super.onStop();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		super.onItemClick(parent, view, position, id);
		this.getListViewModel().setSelectedItem(position);
		((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMSRULE_SELECT.toString());
	}

	@Override
	public int getLayout() {
		return R.layout.alarmruleslist_fragment;
	}

	@Override
	public int getItemLayout() {
		return R.layout.alarmruleslist_item;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}

	@Override
	public Class<AlarmRulesListAdapter> getAdapterClass() {
		return AlarmRulesListAdapter.class;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.add_rule) {
			if (((AlarmViewModel)((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem()).getAgendasList().getSelectedItem() != null) {
				((AlarmSetScreen) this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMSRULE_ADD.toString());
			} else {
				Toast.makeText(v.getContext(), R.string.select_agenda_first, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
