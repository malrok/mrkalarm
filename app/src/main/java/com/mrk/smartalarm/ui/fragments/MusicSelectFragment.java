package com.mrk.smartalarm.ui.fragments;

import android.Manifest;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.dataloaders.MusicDataLoader;
import com.mrk.smartalarm.engine.helpers.SnackBarHelper;
import com.mrk.smartalarm.models.Song;
import com.mrk.smartalarm.ui.adapters.MusicExpandableAdapter;
import com.mrk.smartalarm.ui.views.SongsDialog;
import com.mrk.smartalarm.ui.views.interfaces.SongsDialogDismissListener;
import com.mrk.smartalarm.viewmodels.MusicListViewModel;

import java.util.ArrayList;
import java.util.List;

public class MusicSelectFragment extends Fragment implements ExpandableListView.OnChildClickListener, PermissionListener {

	private ExpandableListView expListView;

	private MusicListViewModel listVm;

	private MusicDataLoader musicDataLoader;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.musicselect_fragment, null);

		expListView = (ExpandableListView) root.findViewById(R.id.exp_list);

		expListView.setOnChildClickListener(this);

		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		new TedPermission(this.getActivity())
				.setPermissionListener(this)
				.setDeniedMessage(getString(R.string.mandatory_read_storage_permission))
				.setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
				.check();
	}

	public void setListViewModel(MusicListViewModel listVm) {
		this.listVm = listVm;

		if (musicDataLoader != null) {
			musicDataLoader.updateArtistsList(listVm);
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
		SongsDialog dialog = new SongsDialog(this.getActivity(), musicDataLoader.getArtists().get(groupPosition).getAlbums().get(childPosition).getSongs());

		dialog.setListener(new SongsDialogDismissListener() {
			@Override
			public void dismiss(List<Song> songs) {
				musicDataLoader.updateViewModel(listVm);
			}
		});

		dialog.show();

		return false;
	}

	@Override
	public void onPermissionGranted() {
		musicDataLoader = new MusicDataLoader();

		musicDataLoader.getArtistsList(this.getActivity());

		if (listVm != null) {
			musicDataLoader.updateArtistsList(listVm);
		}

		MusicExpandableAdapter adapter = new MusicExpandableAdapter(this.getActivity(), musicDataLoader.getArtists());

		expListView.setAdapter(adapter);
	}

	@Override
	public void onPermissionDenied(ArrayList<String> arrayList) {
		SnackBarHelper.make(this.getView(), R.string.mandatory_read_storage_permission, Snackbar.LENGTH_SHORT).show();
	}
}
