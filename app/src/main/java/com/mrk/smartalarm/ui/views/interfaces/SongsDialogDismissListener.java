package com.mrk.smartalarm.ui.views.interfaces;

import com.mrk.smartalarm.models.Song;

import java.util.List;

public interface SongsDialogDismissListener {

	void dismiss(List<Song> songs);

}
