package com.mrk.smartalarm.ui.adapters;

import com.mrk.mrkframework.ui.adapter.AbstractRecyclerViewFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Alarm;

public class AlarmsListAdapter extends AbstractRecyclerViewFragmentAdapter<Alarm> {

	public AlarmsListAdapter(IListViewModel<Alarm> vmList) {
		super(vmList);
	}

	@Override
	public int getItemLayout(int viewType) {
		return R.layout.alarmslist_item;
	}

}
