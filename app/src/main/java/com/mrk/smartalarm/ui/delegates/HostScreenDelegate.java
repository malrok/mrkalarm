package com.mrk.smartalarm.ui.delegates;

import android.app.Activity;
import android.app.Fragment;
import android.widget.Toast;

import com.mrk.mrkframework.ui.fragmentdelegate.AbstractFragmentActivityDelegate;
import com.mrk.mrkframework.ui.fragmentdelegate.WrongFragmentException;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.fragments.AlarmAgendaMasterFragment;
import com.mrk.smartalarm.ui.fragments.AlarmDetailFragment;
import com.mrk.smartalarm.ui.fragments.AlarmMasterFragment;
import com.mrk.smartalarm.ui.fragments.AlarmRingMasterFragment;
import com.mrk.smartalarm.ui.fragments.AlarmRuleFragment;
import com.mrk.smartalarm.ui.fragments.AlarmsListFragment;
import com.mrk.smartalarm.ui.fragments.MusicSelectFragment;
import com.mrk.smartalarm.ui.fragments.SilencesFragment;
import com.mrk.smartalarm.ui.fragments.PreferencesFragment;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.RuleViewModel;

public class HostScreenDelegate extends AbstractFragmentActivityDelegate {

    public HostScreenDelegate(Activity activity, int fragmentContainer) {
        super(activity, fragmentContainer);
    }

    @Override
    public void onBackPressed() {
        try {
            if (getCurrentFragment().equals(getFragmentFromClassName(AlarmsListFragment.class.getName()))) {
                getActivity().finish();
            } else {
                setPreviousFragment();
            }
        } catch (WrongFragmentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Fragment getFragmentFromAction(String action) {
        Fragment nextFragment = null;

        try {
            switch (HostScreenDelegateAction.Actions.valueOf(action)) {
				case ALARMLIST_PUSH:
                    ((AlarmSetScreen) getActivity()).setAlarms();
                    ((AlarmSetScreen) getActivity()).save();
                case ACTIVITY_RESUME:
                    nextFragment = getFragmentFromClassName(AlarmsListFragment.class.getName());
                    ((AlarmsListFragment)nextFragment).setListViewModel(((AlarmSetScreen) getActivity()).getAlarmsViewModel());
                    ((AlarmSetScreen) getActivity()).toggleMenuItems(false);
                    break;
                case ALARMSLIST_SELECT:
                    nextFragment = getFragmentFromClassName(AlarmMasterFragment.class.getName());
                    ((AlarmMasterFragment)nextFragment).setViewModel(((AlarmSetScreen)getActivity()).getAlarmsViewModel().getSelectedItem());
					((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMSLIST_ADD:
                    nextFragment = getFragmentFromClassName(AlarmMasterFragment.class.getName());
                    AlarmViewModel alarmVM = new AlarmViewModel();
                    ((AlarmSetScreen) getActivity()).getAlarmsViewModel().addVM(alarmVM);
                    ((AlarmSetScreen) getActivity()).getAlarmsViewModel().setSelectedItem(alarmVM);
                    ((AlarmMasterFragment)nextFragment).setViewModel(alarmVM);
					((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMMASTER_DETAIL:
                    nextFragment = getFragmentFromClassName(AlarmDetailFragment.class.getName());
                    ((AlarmDetailFragment)nextFragment).setViewModel(((AlarmSetScreen)getActivity()).getAlarmsViewModel().getSelectedItem());
					((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMMASTER_SILENCES:
                    nextFragment = getFragmentFromClassName(SilencesFragment.class.getName());
                    ((SilencesFragment)nextFragment).setListViewModel(
                            ((AlarmViewModel) ((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem()).getSilencesList()
                    );
                    ((SilencesFragment)nextFragment).setMode(SilencesFragment.ALARM_SILENCE);
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMMASTER_AGENDA:
                    nextFragment = getFragmentFromClassName(AlarmAgendaMasterFragment.class.getName());
                    ((AlarmAgendaMasterFragment)nextFragment).setViewModel(((AlarmSetScreen)getActivity()).getAlarmsViewModel().getSelectedItem());
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMAGENDA_SELECT:
                    if (getCurrentFragment() instanceof AlarmAgendaMasterFragment) {
                        ((AlarmAgendaMasterFragment) getCurrentFragment()).setRulesListViewModel(
                                ((AgendaViewModel) ((AlarmViewModel) ((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem())
                                        .getAgendasList().getSelectedItem()).getRulesList()
                        );
                    }
                    break;
                case ALARMSRULE_SELECT:
                    nextFragment = getFragmentFromClassName(AlarmRuleFragment.class.getName());
                    ((AlarmRuleFragment)nextFragment).setViewModel(
                            ((AgendaViewModel)((AlarmViewModel)((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem())
                                    .getAgendasList().getSelectedItem()).getRulesList().getSelectedItem()
                    );
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMSRULE_ADD:
                    nextFragment = getFragmentFromClassName(AlarmRuleFragment.class.getName());
                    RuleViewModel ruleVM = new RuleViewModel();
                    ((AgendaViewModel)((AlarmViewModel)((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem())
                            .getAgendasList().getSelectedItem()).getRulesList().addVM(ruleVM);
                    ((AgendaViewModel)((AlarmViewModel)((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem())
                            .getAgendasList().getSelectedItem()).getRulesList().setSelectedItem(ruleVM);
                    ((AlarmRuleFragment) nextFragment).setViewModel(ruleVM);
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMSRULE_SILENCES:
                    nextFragment = getFragmentFromClassName(SilencesFragment.class.getName());
                    ((SilencesFragment)nextFragment).setListViewModel(
                            ((RuleViewModel) ((AgendaViewModel) ((AlarmViewModel) ((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem())
                                    .getAgendasList().getSelectedItem()).getRulesList().getSelectedItem()).getSilencesList()
                    );
                    ((SilencesFragment)nextFragment).setMode(SilencesFragment.RULE_SILENCE);
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMMASTER_RING:
                    nextFragment = getFragmentFromClassName(AlarmRingMasterFragment.class.getName());
                    ((AlarmRingMasterFragment)nextFragment).setViewModel(((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem());
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case ALARMSMUSIC_ADD:
                    nextFragment = getFragmentFromClassName(MusicSelectFragment.class.getName());
                    ((MusicSelectFragment)nextFragment).setListViewModel(((AlarmViewModel)((AlarmSetScreen) getActivity()).getAlarmsViewModel().getSelectedItem()).getMusicList());
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
                case PREFERENCES_NAVIGATE:
                    nextFragment = getFragmentFromClassName(PreferencesFragment.class.getName());
                    ((AlarmSetScreen)getActivity()).toggleMenuItems(true);
                    break;
            }
        } catch (WrongFragmentException e) {
            Toast.makeText(this.getActivity().getApplicationContext(), "wrong fragment", Toast.LENGTH_SHORT).show();
        }

        return nextFragment;
    }

    @Override
    public void setPreviousFragment() {
        String action = null;
        try {
            if (getCurrentFragment().equals(getFragmentFromClassName(AlarmMasterFragment.class.getName()))) {
				action = HostScreenDelegateAction.Actions.ALARMLIST_PUSH.toString();
			} else if (getCurrentFragment().equals(getFragmentFromClassName(AlarmDetailFragment.class.getName()))) {
				action = HostScreenDelegateAction.Actions.ALARMSLIST_SELECT.toString();
			} else if (getCurrentFragment().equals(getFragmentFromClassName(SilencesFragment.class.getName()))) {
                if (((SilencesFragment)getCurrentFragment()).getMode() == SilencesFragment.ALARM_SILENCE) {
                    action = HostScreenDelegateAction.Actions.ALARMSLIST_SELECT.toString();
                } else {
                    action = HostScreenDelegateAction.Actions.ALARMSRULE_SELECT.toString();
                }
			} else if (getCurrentFragment().equals(getFragmentFromClassName(AlarmAgendaMasterFragment.class.getName()))) {
				action = HostScreenDelegateAction.Actions.ALARMSLIST_SELECT.toString();
            } else if (getCurrentFragment().equals(getFragmentFromClassName(AlarmRuleFragment.class.getName()))) {
                action = HostScreenDelegateAction.Actions.ALARMMASTER_AGENDA.toString();
            } else if (getCurrentFragment().equals(getFragmentFromClassName(AlarmRingMasterFragment.class.getName()))) {
                action = HostScreenDelegateAction.Actions.ALARMSLIST_SELECT.toString();
            } else if (getCurrentFragment().equals(getFragmentFromClassName(MusicSelectFragment.class.getName()))) {
                action = HostScreenDelegateAction.Actions.ALARMMASTER_RING.toString();
            } else if (getCurrentFragment().equals(getFragmentFromClassName(PreferencesFragment.class.getName()))) {
                action = HostScreenDelegateAction.Actions.ACTIVITY_RESUME.toString();
            }
        } catch (WrongFragmentException e) {
            e.printStackTrace();
        }
        if (action != null) {
            this.processAction(action);
        }
    }
}
