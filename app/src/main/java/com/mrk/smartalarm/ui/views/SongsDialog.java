package com.mrk.smartalarm.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ListView;

import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Song;
import com.mrk.smartalarm.ui.adapters.SongsListAdapter;
import com.mrk.smartalarm.ui.views.interfaces.SongsDialogDismissListener;

import java.util.List;

public class SongsDialog extends Dialog implements DialogInterface.OnDismissListener {

	private List<Song> songs;

	private SongsDialogDismissListener listener;

	public SongsDialog(Context context, List<Song> songs) {
		super(context);

		this.songs = songs;

		setContentView(R.layout.songs_dialog);

		ListView listView = (ListView) findViewById(R.id.list);

		listView.setAdapter(new SongsListAdapter(context, this.songs));

		findViewById(R.id.validate).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SongsDialog.this.dismiss();
			}
		});

		this.setOnDismissListener(this);
	}

	public void setListener(SongsDialogDismissListener listener) {
		this.listener = listener;
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (listener != null) {
			listener.dismiss(this.songs);
		}
	}
}
