package com.mrk.smartalarm.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.getpebble.android.kit.PebbleKit;
import com.mrk.mrkframework.ui.AbstractMRKRecyclerViewFragment;
import com.mrk.mrkframework.ui.adapter.BindedRecyclerViewHolder;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.events.ActivateAlarmEvent;
import com.mrk.smartalarm.engine.helpers.ServiceHelper;
import com.mrk.smartalarm.engine.helpers.SnackBarHelper;
import com.mrk.smartalarm.engine.pebble.PebbleCommunicationService;
import com.mrk.smartalarm.engine.pebble.PebbleHelper;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.ui.AlarmSetScreen;
import com.mrk.smartalarm.ui.adapters.AlarmsListAdapter;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.AlarmsListViewModel;

import de.greenrobot.event.EventBus;

public class AlarmsListFragment extends AbstractMRKRecyclerViewFragment<Alarm> implements View.OnClickListener {

    private TextView nextAlarmText;
    private ItemTouchHelper touchHelper;

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("activated".equals(intent.getExtras().getString(BindedRecyclerViewHolder.EXTRA_BINDVALUE))) {
                ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).setAlarms();
                ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).save();
                nextAlarmText.setText(((AlarmsListViewModel) getListViewModel()).getNextAlarmText(getActivity()));
                SnackBarHelper.make(AlarmsListFragment.this.getRootView().findViewById(R.id.main_content), R.string.save_successful, Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        nextAlarmText = (TextView) this.getRootView().findViewById(R.id.nextAlarm);

        this.getRootView().findViewById(R.id.menu_item).setOnClickListener(this);

        touchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE, ItemTouchHelper.START | ItemTouchHelper.END);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                AlarmViewModel vm = (AlarmViewModel) ((AlarmSetScreen) AlarmsListFragment.this.getActivity()).getAlarmsViewModel().getViewModelList().get(viewHolder.getAdapterPosition());
                ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).getAlarmsViewModel().removeVM(vm);
                ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).setAlarms();
                ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).save();
            }
        });

        touchHelper.attachToRecyclerView(getRecycler());

        LocalBroadcastManager.getInstance(this.getActivity()).registerReceiver(messageReceiver, new IntentFilter(BindedRecyclerViewHolder.LIST_ITEM_CHANGED));

        nextAlarmText.setText(((AlarmsListViewModel) getListViewModel()).getNextAlarmText(getActivity()));

        PebbleHelper.checkPebbleOnFirstLaunch(this.getActivity(), new PebbleHelper.PebbleHelperCallback() {
            @Override
            public void onPebbleLinked() {
                if (PebbleKit.isWatchConnected(AlarmsListFragment.this.getActivity().getApplicationContext())) {
                    if (!ServiceHelper.isServiceRunning(AlarmsListFragment.this.getActivity(), PebbleCommunicationService.class)) {
                        ServiceHelper.startService(AlarmsListFragment.this.getActivity(), PebbleCommunicationService.class);
                    }
                }
            }
        });
    }

    @Override
    public void onStop() {
        touchHelper.attachToRecyclerView(null);

        LocalBroadcastManager.getInstance(this.getActivity()).unregisterReceiver(messageReceiver);

        super.onStop();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public int getLayout() {
        return R.layout.alarmslist_fragment;
    }

    @Override
    public int getItemLayout() {
        return R.layout.alarmslist_item;
    }

    @Override
    public Class<AlarmsListAdapter> getAdapterClass() {
        return AlarmsListAdapter.class;
    }

    @Override
    protected boolean copyViewModel() {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        super.onItemClick(parent, view, position, id);
        ((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMSLIST_SELECT.toString());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.menu_item) {
            ((AlarmSetScreen)this.getActivity()).sendActionToDelegate(HostScreenDelegateAction.Actions.ALARMSLIST_ADD.toString());
        }
    }

    // EventBus callback
    public void onEvent(ActivateAlarmEvent event) {
        AlarmViewModel alarmViewModel = ((AlarmsListViewModel) this.getListViewModel()).getViewModel(event.getIdentifier());
        alarmViewModel.getData().setActivated(event.isActivated());

        getRecycler().getAdapter().notifyDataSetChanged();

        ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).setAlarms();
        ((AlarmSetScreen)AlarmsListFragment.this.getActivity()).save();
        nextAlarmText.setText(((AlarmsListViewModel) getListViewModel()).getNextAlarmText(getActivity()));
    }
}
