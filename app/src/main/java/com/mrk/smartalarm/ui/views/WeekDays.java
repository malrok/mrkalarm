package com.mrk.smartalarm.ui.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mrk.mrkframework.databinding.interfaces.BindableView;
import com.mrk.mrkframework.databinding.interfaces.ChangeListener;
import com.mrk.smartalarm.R;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class WeekDays extends LinearLayout implements View.OnClickListener, BindableView {

	public static final int INITIALS = 0;

	public static final int SHORT = 0;

	private View root;

	private boolean[] selection;

	private boolean[] selectedItems;

	/** index of first day of week in locale */
	private int first;

	/** initial of the names of week days in locale. */
	private String[] initialDayNames;

	/** short name of week days in locale */
	private String[] shortDayNames;

	/** long name of week days in locale */
	private String[] longDayNames;

	private AlertDialog dialog;

	/** ids of the views */
	private int[] daysViews = {
			R.id.first_day,
			R.id.second_day,
			R.id.third_day,
			R.id.fourth_day,
			R.id.fifth_day,
			R.id.sixth_day,
			R.id.seventh_day
	};

	private List<ChangeListener> changedListener;

	private int displayType;

	public WeekDays(Context context) {
		this(context, null);
	}

	public WeekDays(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public WeekDays(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		root = inflater.inflate(R.layout.week_days_view, this, true);

		if (attrs != null) {
			String enabledStr = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "enabled");
			boolean enabled = true;
			if (enabledStr != null) {
				enabled = !enabledStr.equals("false");
			}
			if (enabled) {
				root.setOnClickListener(this);
			}

			TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.WeekDays, 0, 0);

			displayType = typedArray.getInt(R.styleable.WeekDays_displayType, 0);

			typedArray.recycle();
		}

		init();
	}

	public void setSelection(byte selection) {
		this.selection = new boolean[7];
		for (int i=0; i<this.selection.length; i++) {
			this.selection[i] = (selection & (int)Math.pow(2, i)) != 0;
		}
		fillDays();
	}

	public byte getSelection() {
		return getByteFromBoolArray(this.selection);
	}

	private byte getByteFromBoolArray(boolean[] bools) {
		byte selected = 0;

		for (int i=0; i<bools.length; i++) {
			selected += bools[i] ? Math.pow(2, i) : 0;
		}

		return selected;
	}

	private void fillDays() {
		SpannableString content;
		for (int i=0; i<7; i++) {
			if (displayType == SHORT) {
				content = new SpannableString(shortDayNames[getDayForIndex(i)]);
			} else {
				content = new SpannableString(initialDayNames[getDayForIndex(i)]);
			}
			if (this.selection != null && this.selection[i]) {
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			}
			((TextView) root.findViewById(daysViews[i])).setText(content);
		}
	}

	private void init() {
		changedListener = new ArrayList<>();

		first = Calendar.getInstance().getFirstDayOfWeek();

		DateFormatSymbols symbols = new DateFormatSymbols();

		shortDayNames = symbols.getShortWeekdays();

		initialDayNames = new String[shortDayNames.length];

		for (int i = 1; i < shortDayNames.length; i++) {
			initialDayNames[i] = shortDayNames[i].substring(0, 1) + ".";
		}

		longDayNames = new String[7];
		String[] temp = symbols.getWeekdays();

		// first element is an empty string
		for (int i = 1; i<temp.length; i++) {
			longDayNames[i - 1] = temp[getDayForIndex(i - 1)];
		}

		fillDays();
	}

	private int getDayForIndex(int index) {
		int day = index + first;
		if (day > 7) {
			day -= 7;
		}
		return day;
	}

	@Override
	public void onClick(View v) {
		if (this.isEnabled()) {
			selectedItems = Arrays.copyOf(this.selection, this.selection.length);
			showDialog(getContext());
		}
	}

	private void showDialog(Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle(R.string.select_days)
				.setMultiChoiceItems(longDayNames, selection,
						new DialogInterface.OnMultiChoiceClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which, boolean isChecked) {
								selectedItems[which] = isChecked;
							}
						})
				.setPositiveButton(R.string.button_valid, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						boolean[] previous = selection;
						selection = selectedItems;
						fillDays();
						notifyChange(previous);
					}
				})
				.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {

					}
				});


		dialog = builder.create();

		dialog.show();
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		Bundle bundle = new Bundle();
		bundle.putParcelable("super", superState);
		bundle.putBoolean("dialog", dialog != null && dialog.isShowing());
		bundle.putBooleanArray("selection", this.selection);
		bundle.putBooleanArray("selectedItems", this.selectedItems);

		return bundle;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Bundle bundle = (Bundle) state;
		super.onRestoreInstanceState(bundle.getParcelable("super"));
		this.selection = bundle.getBooleanArray("selection");
		this.selectedItems = bundle.getBooleanArray("selectedItems");

		fillDays();

		if (bundle.getBoolean("dialog")) {
			showDialog(getContext());
		}
	}

	@Override
	public void addChangeListener(ChangeListener changeListener) {
		this.changedListener.add(changeListener);
	}

	@Override
	public void setValue(Object value) {
		if (value instanceof boolean[]) {
			this.selection = (boolean[]) value;
		} else if (value instanceof Byte) {
			this.setSelection((Byte)value);
		}
	}

	private void notifyChange(boolean[] previous) {
		byte current = getSelection();
		byte prev = getByteFromBoolArray(previous);

		for (ChangeListener listener : changedListener) {
			listener.onValueChanged(current, prev);
		}
	}
}
