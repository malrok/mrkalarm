package com.mrk.smartalarm.ui.views.interfaces;

import com.mrk.smartalarm.viewmodels.SilenceViewModel;

public interface SilencePeriodsListener {

	void remove(SilenceViewModel item);

}
