package com.mrk.smartalarm.ui.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;

import com.mrk.mrkframework.ui.AbstractMRKFragment;
import com.mrk.mrkframework.ui.adapter.interfaces.AdapterLifeCycleInterface;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;
import com.mrk.smartalarm.viewmodels.AgendasListViewModel;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.RulesListViewModel;

public class AlarmAgendaMasterFragment extends AbstractMRKFragment<Alarm> implements AdapterLifeCycleInterface {

	private AlarmAgendaListFragment agendaList;
	private AlarmRulesListFragment rulesList;

	@Override
	public void onStart() {
		super.onStart();

		setFragments();
	}

	@Override
	public void onStop() {
		agendaList.setAdapterCallback(null);

		super.onStop();
	}

	private void setFragments() {
		FragmentTransaction ft = this.getActivity().getFragmentManager().beginTransaction();

		// agendas list
		agendaList = new AlarmAgendaListFragment();
		agendaList.setListViewModel(((AlarmViewModel) this.getViewModel()).getAgendasList());
		agendaList.setAdapterCallback(this);
		ft.replace(R.id.agendasList, agendaList);

		// rules list
		rulesList = new AlarmRulesListFragment();
		ft.replace(R.id.rulesList, rulesList);

		ft.commit();
	}

	private void setAgendaSelection() {
		AgendasListViewModel vm = ((AlarmViewModel)this.getViewModel()).getAgendasList();
		if (!vm.getViewModelList().isEmpty() && agendaList.getSelectedItemsNumber() == 0) {
			agendaList.toggleSelection(0);
			vm.setSelectedItem(0);
			setRulesListViewModel(((AgendaViewModel)vm.getSelectedItem()).getRulesList());
		}
	}

	@Override
	public int getLayout() {
		return R.layout.alarmagendamaster_fragment;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}

	public void setRulesListViewModel(RulesListViewModel listVM) {
		rulesList.setListViewModel(listVM);
	}

	@Override
	public void onAdapterSet(Fragment fragment) {
		setAgendaSelection();
	}
}