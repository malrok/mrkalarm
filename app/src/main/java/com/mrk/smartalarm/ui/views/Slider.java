package com.mrk.smartalarm.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

import com.mrk.smartalarm.R;
import com.mrk.smartalarm.ui.views.interfaces.SliderListener;


public class Slider extends SeekBar implements SeekBar.OnSeekBarChangeListener {

	private static final int STEP = 24;

	private ElasticSlider elasticSlider;
	private boolean isTouched = false;
	private int originalProgress;

	private Drawable looseBackground;
	private Drawable touchedBackground;

	private SliderListener listener;

	public Slider(Context context) {
		super(context);
	}

	public Slider(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public Slider(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		elasticSlider = new ElasticSlider();
		elasticSlider.sleep(0);

		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.Slider, defStyle, 0);

		looseBackground = array.getDrawable(R.styleable.Slider_looseBackground);
		if (looseBackground == null) {
			looseBackground = ContextCompat.getDrawable(context, R.drawable.loose_slider_background);
		}

		touchedBackground = array.getDrawable(R.styleable.Slider_touchedBackground);
		if (touchedBackground == null) {
			touchedBackground = ContextCompat.getDrawable(context, R.drawable.touched_slider_background);
		}

		Drawable thumbDrawable = array.getDrawable(R.styleable.Slider_android_thumb);
		if (thumbDrawable == null) {
			thumbDrawable = ContextCompat.getDrawable(context, R.drawable.slider_thumb);
		}

		setThumb(thumbDrawable);

		setProgressDrawable(null);
		setBackgroundDrawable(looseBackground);

		array.recycle();

		this.setOnSeekBarChangeListener(this);
	}

	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			setBackgroundDrawable(touchedBackground);
			isTouched = true;
			if (elasticSlider != null)
				elasticSlider.removeMessages(0);
			break;
		case MotionEvent.ACTION_UP:
			isTouched = false;
			if (elasticSlider != null)
				elasticSlider.sleep(0);
			break;
		}
		return super.onTouchEvent(event);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (fromUser) {
			if (progress < (originalProgress - STEP)) {
				setProgress(originalProgress);
			} else {
				if (progress >= getMax()) {
					if (elasticSlider != null) {
						elasticSlider.removeMessages(0);
						elasticSlider = null;
					}

					if (listener != null) {
						listener.onMaxReached();
					}
				} else {
					if (listener != null) {
						listener.onProgress(progress);
					}
				}

				originalProgress = progress;
			}
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		originalProgress = seekBar.getProgress();
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	public void setSliderListener(SliderListener listener) {
		this.listener = listener;
	}

	private class ElasticSlider extends Handler {
		@Override
		public void handleMessage(Message msg) {
			elasticFunction();
		}

		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	}

	private void elasticFunction() {
		if (!isTouched)
			setProgress(getProgress() - 8);

		if (getProgress() <= 0) {
			setBackgroundDrawable(looseBackground);
		}

		elasticSlider.sleep(1);
	}
}
