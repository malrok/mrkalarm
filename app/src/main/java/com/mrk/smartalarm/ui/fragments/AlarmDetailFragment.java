package com.mrk.smartalarm.ui.fragments;

import com.mrk.mrkframework.ui.AbstractMRKFragment;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Alarm;

public class AlarmDetailFragment extends AbstractMRKFragment<Alarm> {

	@Override
	public int getLayout() {
		return R.layout.alarmdetail_fragment;
	}

	@Override
	protected boolean copyViewModel() {
		return false;
	}
}
