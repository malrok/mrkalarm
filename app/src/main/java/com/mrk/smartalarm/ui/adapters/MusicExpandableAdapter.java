package com.mrk.smartalarm.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Artist;

import java.util.List;

public class MusicExpandableAdapter extends BaseExpandableListAdapter {

	private Context context;
	private List<Artist> artists;

	public MusicExpandableAdapter(Context context, List<Artist> artists) {
		this.context = context;
		this.artists = artists;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	public Object getChild(int gPosition, int cPosition) {
		return artists.get(gPosition).getAlbums().get(cPosition);
	}

	public long getChildId(int gPosition, int cPosition) {
		return cPosition;
	}

	// album view
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);

			rowView = inflater.inflate(R.layout.musiclist_album_item, null);

			ChildViewHolder viewHolder = new ChildViewHolder();
			viewHolder.textViewChild = (TextView) rowView.findViewById(R.id.name);
			viewHolder.checkBoxChild = (CheckBox) rowView.findViewById(R.id.selected);

			rowView.setTag(viewHolder);
		}

		ChildViewHolder holder = (ChildViewHolder) rowView.getTag();

		holder.textViewChild.setText(artists.get(groupPosition).getAlbums().get(childPosition).getName());

		return rowView;
	}

	public int getChildrenCount(int gPosition) {
		return artists.get(gPosition).getAlbums().size();
	}

	public Object getGroup(int gPosition) {
		return artists.get(gPosition);
	}

	public int getGroupCount() {
		return artists.size();
	}

	public long getGroupId(int gPosition) {
		return gPosition;
	}

	// artist view
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);

			rowView = inflater.inflate(R.layout.musiclist_artist_item, null);

			GroupViewHolder viewHolder = new GroupViewHolder();
			viewHolder.textViewGroup = (TextView) rowView.findViewById(R.id.name);
			viewHolder.checkBoxGroup = (CheckBox) rowView.findViewById(R.id.selected);

			rowView.setTag(viewHolder);
		}

		GroupViewHolder holder = (GroupViewHolder) rowView.getTag();

		holder.textViewGroup.setText(artists.get(groupPosition).getName());
//		holder.checkBoxGroup.setChecked(artists.get(groupPosition).);

		return rowView;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

	class GroupViewHolder {
		public TextView textViewGroup;
		public CheckBox checkBoxGroup;
	}

	class ChildViewHolder {
		public TextView textViewChild;
		public ImageView imageViewChild;
		public CheckBox checkBoxChild;
	}
}
