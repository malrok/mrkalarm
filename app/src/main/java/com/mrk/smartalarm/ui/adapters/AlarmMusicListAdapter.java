package com.mrk.smartalarm.ui.adapters;

import com.mrk.mrkframework.ui.adapter.AbstractRecyclerViewFragmentAdapter;
import com.mrk.mrkframework.viewmodel.interfaces.IListViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.models.Music;

public class AlarmMusicListAdapter extends AbstractRecyclerViewFragmentAdapter<Music> {

//	private IListViewModel<Music> vmList;

	public AlarmMusicListAdapter(IListViewModel<Music> vmList) {
		super(vmList);
	}

	@Override
	public int getItemLayout(int viewType) {
		return R.layout.alarmmusiclist_fragment;
	}

}
