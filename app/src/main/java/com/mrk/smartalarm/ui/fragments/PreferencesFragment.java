package com.mrk.smartalarm.ui.fragments;

import android.Manifest;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.design.widget.Snackbar;

import com.getpebble.android.kit.PebbleKit;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.helpers.SnackBarHelper;

import java.util.ArrayList;

public class PreferencesFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener, PermissionListener {

	public static final String DEBUG_PREFERENCE = "debug_preference";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);

		if (!PebbleKit.isWatchConnected(getActivity().getApplicationContext())) {
			PreferenceScreen preferenceScreen = (PreferenceScreen) findPreference("preference_screen");
			PreferenceCategory pebbleCategory = (PreferenceCategory) findPreference("pebble_category");
			preferenceScreen.removePreference(pebbleCategory);
		}

		findPreference(DEBUG_PREFERENCE).setOnPreferenceChangeListener(this);
	}


	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		if (preference.getKey().equals(DEBUG_PREFERENCE)) {
			new TedPermission(this.getActivity())
					.setPermissionListener(this)
					.setDeniedMessage(getString(R.string.mandatory_write_storage_permission))
					.setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
					.check();
		}
		return true;
	}

	@Override
	public void onPermissionGranted() {
		// M'kay
	}

	@Override
	public void onPermissionDenied(ArrayList<String> arrayList) {
		findPreference(DEBUG_PREFERENCE).getEditor().putBoolean(DEBUG_PREFERENCE, false);
		SnackBarHelper.make(this.getActivity(), R.string.mandatory_write_storage_permission, Snackbar.LENGTH_SHORT).show();
	}
}
