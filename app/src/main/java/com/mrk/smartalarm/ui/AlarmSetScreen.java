package com.mrk.smartalarm.ui;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.extras.toolbar.MaterialMenuIconToolbar;
import com.mrk.mrkframework.ui.AbstractMRKFragmentActivity;
import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.FragmentActivityDelegate;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.dataloaders.AlarmsDataLoader;
import com.mrk.smartalarm.engine.broadcasts.BroadcastsHelper;
import com.mrk.smartalarm.engine.helpers.AlarmsHelper;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegate;
import com.mrk.smartalarm.ui.delegates.HostScreenDelegateAction;
import com.mrk.smartalarm.ui.fragments.AlarmAgendaMasterFragment;
import com.mrk.smartalarm.ui.fragments.AlarmDetailFragment;
import com.mrk.smartalarm.ui.fragments.AlarmMasterFragment;
import com.mrk.smartalarm.ui.fragments.AlarmRingMasterFragment;
import com.mrk.smartalarm.ui.fragments.AlarmRuleFragment;
import com.mrk.smartalarm.ui.fragments.AlarmsListFragment;
import com.mrk.smartalarm.ui.fragments.MusicSelectFragment;
import com.mrk.smartalarm.ui.fragments.PreferencesFragment;
import com.mrk.smartalarm.ui.fragments.SilencesFragment;
import com.mrk.smartalarm.viewmodels.AlarmsListViewModel;

public class AlarmSetScreen extends AbstractMRKFragmentActivity {

	private MaterialMenuIconToolbar materialMenu;

    private AlarmsListViewModel alarmsViewModel;

    private boolean menuCreated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_set_screen);

		Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
		if (toolBar != null) {
			setSupportActionBar(toolBar);
			materialMenu = new MaterialMenuIconToolbar(this, Color.WHITE, MaterialMenuDrawable.Stroke.THIN) {
				@Override public int getToolbarViewId() {
					return R.id.toolbar;
				}
			};
		}
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (this.alarmsViewModel == null) {
            load();
        }

        if (getCurrentFragment() == null) {
            sendActionToDelegate(HostScreenDelegateAction.Actions.ACTIVITY_RESUME.toString());
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        materialMenu.syncState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("alarmsViewModel", getAlarmsViewModel());
        materialMenu.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.alarmsViewModel = savedInstanceState.getParcelable("alarmsViewModel");

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menuCreated = true;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setPreviousFragment();
                break;
            case R.id.action_settings:
                sendActionToDelegate(HostScreenDelegateAction.Actions.PREFERENCES_NAVIGATE.toString());
                break;
        }
        return true;
    }

    @Override
    public Class<? extends Fragment>[] getFragmentsList() {
        return new Class[] {
                AlarmsListFragment.class,
                AlarmMasterFragment.class,
                AlarmDetailFragment.class,
                SilencesFragment.class,
				AlarmAgendaMasterFragment.class,
                AlarmRuleFragment.class,
                AlarmRingMasterFragment.class,
                MusicSelectFragment.class,
                PreferencesFragment.class
        };
    }

    @Override
    public FragmentActivityDelegate setDelegate() {
        return new HostScreenDelegate(this, R.id.fragment_container);
    }

    public void toggleMenuItems(final boolean homeAsUp) {
        if (menuCreated) {
            materialMenu.animatePressedState(homeAsUp ? MaterialMenuDrawable.IconState.ARROW : MaterialMenuDrawable.IconState.BURGER);
        }
    }

    private void load() {
        this.alarmsViewModel = new AlarmsListViewModel();
        this.alarmsViewModel.loadFromDataLoader(this, new AlarmsDataLoader(this));
    }

    public void save() {
        new AlarmsDataLoader(this).pushFromViewModel(this, this.alarmsViewModel);
    }

    public void setAlarms() {
        // set next alarm
        AlarmsHelper.setAlarms(this, this.alarmsViewModel);
        BroadcastsHelper.sendNextAlarmBroadcast(this, this.alarmsViewModel);

        // set next silence
        AlarmsHelper.setNextSilence(this, alarmsViewModel);
    }

    public AlarmsListViewModel getAlarmsViewModel() {
        return this.alarmsViewModel;
    }
}
