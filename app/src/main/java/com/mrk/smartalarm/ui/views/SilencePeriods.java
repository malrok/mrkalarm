package com.mrk.smartalarm.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.appyvet.rangebar.RangeBar;
import com.mrk.mrkframework.ui.helper.DensityPixelHelper;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.helpers.DaysOfTheWeekHelper;
import com.mrk.smartalarm.models.Silence;
import com.mrk.smartalarm.ui.formatters.SilenceBarFormatter;
import com.mrk.smartalarm.ui.views.interfaces.SilencePeriodsListener;
import com.mrk.smartalarm.viewmodels.SilenceViewModel;
import com.mrk.smartalarm.viewmodels.SilencesListViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SilencePeriods extends LinearLayout {

	private Map<RangeBar, SilenceViewModel> viewsVmMap;

	private Paint paint;

	private List<SilencePeriodsListener> listeners;

	private Map<Byte, Integer> displayedDays;

	public SilencePeriods(Context context) {
		this(context, null);
	}

	public SilencePeriods(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SilencePeriods(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init();
	}

	private void init() {
		this.setOrientation(VERTICAL);
		setWillNotDraw(false);

		paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setStrokeWidth(2);

		this.listeners = new ArrayList<>();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		int hourWidth = (this.getWidth() - DensityPixelHelper.dpToPx(this.getContext(), 28)) / 40;

		canvas.drawLine(DensityPixelHelper.dpToPx(this.getContext(), 14) + hourWidth * 4, 0,
				DensityPixelHelper.dpToPx(this.getContext(), 14) + hourWidth * 4, this.getHeight(), paint);
		canvas.drawLine(DensityPixelHelper.dpToPx(this.getContext(), 14) + hourWidth * 28, 0,
				DensityPixelHelper.dpToPx(this.getContext(), 14) + hourWidth * 28, this.getHeight(), paint);

		super.onDraw(canvas);
	}

	public void setSilences(SilencesListViewModel listVm) {
		this.viewsVmMap = new HashMap<>();
		this.displayedDays = new HashMap<>();

		List<IViewModel<Silence>> silences = listVm.getViewModelList();
		Collections.sort(silences, new SilenceComparator());

		updateDisplay(silences);

		sortByValues(this.viewsVmMap);
	}

	public void addSilence(SilenceViewModel vm) {
		View silence = getSilenceView(vm);

		if (this.displayedDays.containsKey(vm.getData().getDays())) {
			this.addView(silence, this.displayedDays.get(vm.getData().getDays()));
			incrementDayViewsIndex(vm.getData().getDays());
		} else {
			this.addView(getDaysView(vm.getData().getDays(), this.getChildCount()));
			this.addView(silence);
		}

		sortByValues(this.viewsVmMap);
	}

	public void registerListener(SilencePeriodsListener listener) {
		this.listeners.add(listener);
	}

	public void unregisterListener(SilencePeriodsListener listener) {
		this.listeners.remove(listener);
	}

	private void updateDisplay(List<IViewModel<Silence>> values) {
		this.removeAllViews();

		byte currentDays = -1;

		int currentIndex = 0;

		for (IViewModel<Silence> silence : values) {
			if (currentDays == -1) {
				currentDays = silence.getData().getDays();
				this.addView(getDaysView(currentDays, currentIndex++));
			} else {
				if (silence.getData().getDays() != currentDays) {
					currentDays = silence.getData().getDays();
					this.addView(getDaysView(currentDays, currentIndex++));
				}
			}
			this.addView(getSilenceView(silence));
			currentIndex++;
		}

		invalidate();
	}

	private View getSilenceView(IViewModel<Silence> silence) {
		LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View root = inflater.inflate(R.layout.alarmsilences_silenceperiod, null, true);

		RangeBar silencePeriodBar = (RangeBar) root.findViewById(R.id.silence_period);
		silencePeriodBar.setClickable(false);
		silencePeriodBar.setTickStart(-16);
		silencePeriodBar.setTickEnd(144);
		silencePeriodBar.setDrawTicks(false);
		silencePeriodBar.setTemporaryPins(false);
		silencePeriodBar.setRangePinsByValue(silence.getData().getBeginTime() / 15, silence.getData().getEndTime() / 15);
		silencePeriodBar.setFormatter(new SilenceBarFormatter());

		int color = R.color.vibrateTint;

		switch (silence.getData().getSilenceMode()) {
			case 0:
				color = ContextCompat.getColor(this.getContext(), R.color.vibrateTint);
				break;
			case 1:
				color = ContextCompat.getColor(this.getContext(), R.color.silenceTint);
				break;
			case 2:
				color = ContextCompat.getColor(this.getContext(), R.color.favoriteTint);
				break;
		}

		silencePeriodBar.setPinTextColor(ContextCompat.getColor(this.getContext(), R.color.pinTextColor));
		silencePeriodBar.setPinColor(color);
		silencePeriodBar.setConnectingLineColor(color);

		silencePeriodBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
			@Override
			public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
				SilenceViewModel vm = SilencePeriods.this.viewsVmMap.get(rangeBar);
				if (vm != null) {
					vm.getData().setBeginTime((short) (Integer.valueOf(leftPinValue) * 15));
					vm.getData().setEndTime((short) (Integer.valueOf(rightPinValue) * 15));
				} else {
					Log.e(SilencePeriods.class.getSimpleName(), "RangeBar: Could not find associated VM");
				}
			}
		});

		this.viewsVmMap.put(silencePeriodBar, (SilenceViewModel) silence);

		ImageButton delete = (ImageButton) root.findViewById(R.id.delete);
		delete.setTag(silencePeriodBar);
		delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SilenceViewModel vm = SilencePeriods.this.viewsVmMap.get(v.getTag());
				if (vm != null) {
					notifyRemovalToListeners(vm);
				} else {
					Log.e(SilencePeriods.class.getSimpleName(), "ImageButton: Could not find associated VM");
				}
			}
		});

		return root;
	}

	private View getDaysView(byte currentDays, int index) {
		this.displayedDays.put(currentDays, index);

		WeekDays daysPeriod = new WeekDays(this.getContext());
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		daysPeriod.setLayoutParams(params);
		daysPeriod.setSelection(currentDays);
		daysPeriod.setEnabled(false);

		return daysPeriod;
	}

	private void incrementDayViewsIndex(byte weekDays) {
		boolean increment = false;
		for (Map.Entry<Byte, Integer> weekDaysEntry : this.displayedDays.entrySet()) {
			if (weekDaysEntry.getKey().equals(weekDays)) {
				increment = true;
			}
			if (increment) {
				int index = this.displayedDays.get(weekDaysEntry.getKey()) + 1;
				this.displayedDays.put(weekDaysEntry.getKey(), index);
			}
		}
	}

	// comparators

	private class SilenceComparator implements Comparator<IViewModel<Silence>> {

		@Override
		public int compare(IViewModel<Silence> lhs, IViewModel<Silence> rhs) {
			return silenceVmCompare(lhs, rhs);
		}

	}

	private Map<RangeBar,SilenceViewModel> sortByValues(final Map<RangeBar,SilenceViewModel> map) {

		Comparator<RangeBar> valueComparator =  new Comparator<RangeBar>() {
			public int compare(RangeBar k1, RangeBar k2) {
				SilenceViewModel vm1 = map.get(k1);
				SilenceViewModel vm2 = map.get(k2);

				return silenceVmCompare(vm1, vm2);
			}
		};

		Map<RangeBar,SilenceViewModel> sortedByValues = new TreeMap<>(valueComparator);
		sortedByValues.putAll(map);

		return sortedByValues;
	}

	private int silenceVmCompare(IViewModel<Silence> vm1, IViewModel<Silence> vm2) {
		if (vm1.getData().getDays() == DaysOfTheWeekHelper.DAYS_OF_THE_WEEK) {
			return 1;
		} else {
			if (vm2.getData().getDays() == DaysOfTheWeekHelper.DAYS_OF_THE_WEEK) {
				return -1;
			} else {
				if (vm1.getData().getDays() == DaysOfTheWeekHelper.WEEK_END_DAYS) {
					return 1;
				} else if (vm2.getData().getDays() == DaysOfTheWeekHelper.WEEK_END_DAYS) {
					return -1;
				} else if (vm1.getData().getDays() == vm2.getData().getDays()){
					return 0;
				} else {
					return -1;
				}
			}
		}
	}

	// listeners notification

	private void notifyRemovalToListeners(SilenceViewModel vm) {
		for (SilencePeriodsListener listener : this.listeners) {
			listener.remove(vm);
		}
	}

}
