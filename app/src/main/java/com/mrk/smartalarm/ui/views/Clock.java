package com.mrk.smartalarm.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mrk.smartalarm.R;

import java.util.Calendar;


public class Clock extends LinearLayout {

	private Context context;

//	private int[] colors = { 0xff737373, 0x99737373, 0x88737373, 0x77737373, 0x66737373, 0x55737373, 0x44737373, 0x33737373, 0x22737373, 0x00737373 };

	private int color;

	private int colorRank = 9;

	private TextView textHour;
	private TextView textSeparator;
	private TextView textMinute;
	private TextView textAMPM;

	private Ticker ticker;

	public Clock(Context context) {
		this(context, null);
		this.context = context;
	}

	public Clock(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		this.context = context;
	}

	public Clock(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;

		if (attrs != null) {
			TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Clock, 0, 0);

			color = typedArray.getColor(R.styleable.Clock_digitsColor, ContextCompat.getColor(context, R.color.lightGrey));

			typedArray.recycle();
		}

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.clock, this, true);

		textHour = (TextView) findViewById(R.id.clock_layout_hour);
		textSeparator = (TextView) findViewById(R.id.clock_layout_separator);
		textMinute = (TextView) findViewById(R.id.clock_layout_minutes);
		textAMPM = (TextView) findViewById(R.id.clock_layout_ampm);

		textHour.setTextColor(color);
		textSeparator.setTextColor(color);
		textMinute.setTextColor(color);
		textAMPM.setTextColor(color);

		if (!isInEditMode()) {
			setTime();
			ticker = new Ticker();
			ticker.sleep();
		} else {
			textHour.setText("13");
			textMinute.setText("37");
		}


	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		ticker.stop();
	}

	private class Ticker extends Handler {
		private static final int TICKER = 100;

		@Override
		public void handleMessage(Message msg) {
			setTime();
			sleep();
		}

		public void sleep() {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), TICKER);
		}

		public void stop() {
			this.removeMessages(0);
		}
	}

	private void setTime() {
		Calendar now = Calendar.getInstance();
		String currentTime = DateFormat.getTimeFormat(context).format(now.getTime());
		textHour.setText(currentTime.substring(0, currentTime.indexOf(":")).length() < 2 ?
				"0" + currentTime.substring(0, currentTime.indexOf(":")) :
				currentTime.substring(0, currentTime.indexOf(":")));
		textSeparator.setTextColor(color);
		colorRank++;
		textSeparator.setTextColor(textSeparator.getTextColors().withAlpha(255 / colorRank));
		if (colorRank > 9)
			colorRank = 0;
		if (currentTime.contains(" ")) {
			textMinute.setText(currentTime.substring(currentTime.indexOf(":") + 1, currentTime.indexOf(" ")));
			textAMPM.setText(currentTime.substring(currentTime.indexOf(" ") + 1));
		} else {
			textMinute.setText(currentTime.substring(currentTime.indexOf(":") + 1));
		}
	}
}
