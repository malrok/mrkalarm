package com.mrk.smartalarm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;

import java.util.Calendar;
import java.util.Date;

public class Rule implements ModelInterface {

    private int identifier;
    private int alarmIdentifier;
    private int agendaIdentifier;

    /* calendar entry regex */
    private String regex = "";
    /* rule mode ; 0: wont ring ; 1: ring on rule time ; 2: ring on calendar entry time */
    private RuleMode mode = RuleMode.NO_RING;
    private Date time;

    public Rule() {
        super();

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY,8);
		cal.set(Calendar.MINUTE,30);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);

		time = cal.getTime();
    }

    private Rule(Parcel in) {
        identifier = in.readInt();
        alarmIdentifier = in.readInt();
        agendaIdentifier = in.readInt();
        regex = in.readString();
        mode = (RuleMode) in.readSerializable();
        time = new Date(in.readLong());
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public int getAlarmIdentifier() {
        return alarmIdentifier;
    }

    public void setAlarmIdentifier(int alarmIdentifier) {
        this.alarmIdentifier = alarmIdentifier;
    }

    public int getAgendaIdentifier() {
        return agendaIdentifier;
    }

    public void setAgendaIdentifier(int agendaIdentifier) {
        this.agendaIdentifier = agendaIdentifier;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return this.regex;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean willRing() {
        return mode != RuleMode.NO_RING;
    }

	public RuleMode getMode() {
		return mode;
	}

	public void setMode(RuleMode mode) {
		this.mode = mode;
	}

	@Override
    public Rule copy() {
        Rule rule = new Rule();

        rule.setIdentifier(this.identifier);
        rule.setAlarmIdentifier(this.alarmIdentifier);
        rule.setAgendaIdentifier(this.agendaIdentifier);
        rule.setRegex(this.regex);
        rule.setMode(this.mode);
        rule.setTime(this.time);

        return rule;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(identifier);
        parcel.writeInt(alarmIdentifier);
        parcel.writeInt(agendaIdentifier);
        parcel.writeString(regex);
        parcel.writeSerializable(mode);
        parcel.writeLong(time.getTime());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Rule createFromParcel(Parcel in) {
            return new Rule(in);
        }

        public Rule[] newArray(int size) {
            return new Rule[size];
        }
    };
}

