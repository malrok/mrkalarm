package com.mrk.smartalarm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;

public class Music implements ModelInterface {

    private int identifier;
    private int alarmIdentifier;
    private String path;
    private String name;
    private String artist;
    private long albumId = 0;

    public Music() {
        super();
    }

    private Music(Parcel in) {
        this.identifier = in.readInt();
        this.alarmIdentifier = in.readInt();
        this.path = in.readString();
        this.artist = in.readString();
        this.name = in.readString();
        this.albumId = in.readLong();
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public int getAlarmIdentifier() {
        return alarmIdentifier;
    }

    public void setAlarmIdentifier(int alarmIdentifier) {
        this.alarmIdentifier = alarmIdentifier;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(long l) {
        this.albumId = l;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public Music copy() {
        Music music = new Music();

        music.setIdentifier(this.identifier);
        music.setAlarmIdentifier(this.alarmIdentifier);
        music.setPath(this.path);
        music.setName(this.name);
        music.setArtist(this.artist);
        music.setAlbumId(this.albumId);

        return music;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.identifier);
        parcel.writeInt(this.alarmIdentifier);
        parcel.writeString(this.path);
        parcel.writeString(this.artist);
        parcel.writeString(this.name);
        parcel.writeLong(this.albumId);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Music createFromParcel(Parcel in) {
            return new Music(in);
        }

        public Music[] newArray(int size) {
            return new Music[size];
        }
    };
}
