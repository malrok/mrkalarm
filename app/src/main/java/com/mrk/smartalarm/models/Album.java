package com.mrk.smartalarm.models;

import java.util.ArrayList;
import java.util.List;

public class Album {

	private Long id;

	private String name;

	private List<Song> songs;

	public Album() {
		this.songs = new ArrayList<>();
	}

	public Album(Long id) {
		this();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public void addSong(Song song) {
		this.songs.add(song);
	}
}
