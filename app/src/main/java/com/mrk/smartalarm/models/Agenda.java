package com.mrk.smartalarm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;

public class Agenda implements ModelInterface {

	private int identifier;
	private int alarmIdentifier;
	private String title;
	private int priority;

	public Agenda() {
		// nothing to do
	}

	private Agenda(Parcel in) {
		identifier = in.readInt();
		title = in.readString();
		priority = in.readInt();
	}

	@Override
	public int getIdentifier() {
		return identifier;
	}

	@Override
	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}

	public int getAlarmIdentifier() {
		return alarmIdentifier;
	}

	public void setAlarmIdentifier(int alarmIdentifier) {
		this.alarmIdentifier = alarmIdentifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Override
	public Agenda copy() {
		Agenda agenda = new Agenda();

		agenda.setIdentifier(this.identifier);
		agenda.setAlarmIdentifier(this.alarmIdentifier);
		agenda.setTitle(this.title);
		agenda.setPriority(this.priority);

		return agenda;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(identifier);
		parcel.writeString(title);
		parcel.writeInt(priority);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Agenda createFromParcel(Parcel in) {
			return new Agenda(in);
		}

		public Agenda[] newArray(int size) {
			return new Agenda[size];
		}
	};
}
