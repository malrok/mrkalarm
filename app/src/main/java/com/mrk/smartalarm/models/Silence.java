package com.mrk.smartalarm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.smartalarm.engine.helpers.DaysOfTheWeekHelper;
import com.mrk.smartalarm.models.interfaces.HasDaysInterface;

public class Silence implements ModelInterface, HasDaysInterface {

    private int identifier;
    private int linkedIdentifier;

    private boolean alarmLinked = true;

    // 0: vibrate ; 1: silence ; 2: favorites
    private int silenceMode = 0;
    private byte days;
    // time in minutes; negative value is in the past
    private short beginTime;
    private short endTime;

    public Silence() {
        super();
    }

    private Silence(Parcel in) {
        identifier = in.readInt();
        linkedIdentifier = in.readInt();
		silenceMode = in.readInt();
		days = in.readByte();
		beginTime = (short) in.readInt();
		endTime = (short) in.readInt();
		alarmLinked = in.readByte() == 1;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public int getLinkedIdentifier() {
        return linkedIdentifier;
    }

    public void setLinkedIdentifier(int linkedIdentifier) {
        this.linkedIdentifier = linkedIdentifier;
    }

    public int getSilenceMode() {
        return silenceMode;
    }

    public void setSilenceMode(int silenceMode) {
        this.silenceMode = silenceMode;
    }

	public byte getDays() {
		return days;
	}

	public void setDays(byte days) {
		this.days = days;
	}

    public boolean isMonday() {
        return (this.days & DaysOfTheWeekHelper.MONDAY) != 0;
    }

    public boolean isTuesday() {
        return (this.days & DaysOfTheWeekHelper.TUESDAY) != 0;
    }

    public boolean isWednesday() {
        return (this.days & DaysOfTheWeekHelper.WEDNESDAY) != 0;
    }

    public boolean isThursday() {
        return (this.days & DaysOfTheWeekHelper.THURSDAY) != 0;
    }

    public boolean isFriday() {
        return (this.days & DaysOfTheWeekHelper.FRIDAY) != 0;
    }

    public boolean isSaturday() {
        return (this.days & DaysOfTheWeekHelper.SATURDAY) != 0;
    }

    public boolean isSunday() {
        return (this.days & DaysOfTheWeekHelper.SUNDAY) != 0;
    }

	public short getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(short beginTime) {
		this.beginTime = beginTime;
	}

	public short getEndTime() {
		return endTime;
	}

	public void setEndTime(short endTime) {
		this.endTime = endTime;
	}

    public boolean isAlarmLinked() {
        return alarmLinked;
    }

    public void setAlarmLinked(boolean alarmLinked) {
        this.alarmLinked = alarmLinked;
    }

    @Override
    public Silence copy() {
        Silence silence = new Silence();

		silence.setIdentifier(this.identifier);
		silence.setLinkedIdentifier(this.linkedIdentifier);
		silence.setAlarmLinked(this.alarmLinked);
        silence.setSilenceMode(this.silenceMode);
		silence.setDays(this.getDays());
		silence.setBeginTime(this.getBeginTime());
		silence.setEndTime(this.getEndTime());

        return silence;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(identifier);
        parcel.writeInt(linkedIdentifier);
		parcel.writeInt(silenceMode);
		parcel.writeByte(days);
		parcel.writeInt(beginTime);
		parcel.writeInt(endTime);
		parcel.writeByte((byte) (this.alarmLinked ? 1 : 0));
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Silence createFromParcel(Parcel in) {
            return new Silence(in);
        }

        public Silence[] newArray(int size) {
            return new Silence[size];
        }
    };
}
