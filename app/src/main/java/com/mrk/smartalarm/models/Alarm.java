package com.mrk.smartalarm.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.smartalarm.engine.helpers.DaysOfTheWeekHelper;
import com.mrk.smartalarm.models.interfaces.HasDaysInterface;

import java.util.Calendar;
import java.util.Date;

public class Alarm implements ModelInterface, HasDaysInterface {

    /* rang de l'alarme */
    private int identifier;
    /* nom de l'alarme */
    private String name = "";
    /* supprimer apres usage */
    private boolean deleteAfterUse = false;
    /* will it ring ? */
    private boolean willRing = true;
    /* heure de l'alarme */
    private Date time;
    /* jour selectionnes pour l'activation */
    private byte days = DaysOfTheWeekHelper.MONDAY + DaysOfTheWeekHelper.TUESDAY + DaysOfTheWeekHelper.WEDNESDAY + DaysOfTheWeekHelper.THURSDAY + DaysOfTheWeekHelper.FRIDAY;
    /* alarme activee ou non */
    private boolean activated = false;
    /* alarme en attente ou non */
    private boolean set = false;
    /* repetition */
    private int snoozeTime = 5;
    private boolean snoozeShake = false;
    private int maxSnooze = 5;
    /* alarm type */
    private boolean vibrate = false;
    private int alarmType = 0; // 0 : sound ; 1 : music
    private boolean volumeProgression = true;
    private int alarmDuration = 5;
    private int alarmMaxVolume = 15;

    private Long nextAlarm;

    public Alarm() {
        super();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,8);
        cal.set(Calendar.MINUTE,30);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);

        time = cal.getTime();
    }

    private Alarm(Parcel in) {
        this.deleteAfterUse = in.readByte() != 0;
        this.identifier = in.readInt();
        this.name = in.readString();
        this.willRing = in.readByte() != 0;
        this.time = new Date(in.readLong());
        this.days = in.readByte();
        this.activated = in.readByte() != 0;
        this.set = in.readByte() != 0;
        this.snoozeTime = in.readInt();
        this.snoozeShake = in.readByte() != 0;
        this.maxSnooze = in.readInt();
        this.vibrate = in.readByte() != 0;
        this.alarmType = in.readInt();
        this.volumeProgression = in.readByte() != 0;
        this.alarmDuration = in.readInt();
        this.alarmMaxVolume = in.readInt();
        this.nextAlarm = in.readLong();
    }

    public Alarm(int rnk) {
        this.identifier = rnk;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleteAfterUse() {
        return deleteAfterUse;
    }

    public void setDeleteAfterUse(boolean deleteAfterUse) {
        this.deleteAfterUse = deleteAfterUse;
    }

    public boolean isWillRing() {
        return willRing;
    }

    public void setWillRing(boolean willRing) {
        this.willRing = willRing;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public byte getDays() {
        return days;
    }

    public void setDays(byte days) {
        this.days = days;
    }

    public boolean isMonday() {
        return (this.days & DaysOfTheWeekHelper.MONDAY) != 0;
    }

    public void setMonday(boolean monday) {
        if (monday) {
            this.days += DaysOfTheWeekHelper.MONDAY;
        } else if (isMonday()){
            this.days -= DaysOfTheWeekHelper.MONDAY;
        }
    }

    public boolean isTuesday() {
        return (this.days & DaysOfTheWeekHelper.TUESDAY) != 0;
    }

    public void setTuesday(boolean tuesday) {
        if (tuesday) {
            this.days += DaysOfTheWeekHelper.TUESDAY;
        } else if (isTuesday()) {
            this.days -= DaysOfTheWeekHelper.TUESDAY;
        }
    }

    public boolean isWednesday() {
        return (this.days & DaysOfTheWeekHelper.WEDNESDAY) != 0;
    }

    public void setWednesday(boolean wednesday) {
        if (wednesday) {
            this.days += DaysOfTheWeekHelper.WEDNESDAY;
        } else if (isWednesday()) {
            this.days -= DaysOfTheWeekHelper.WEDNESDAY;
        }
    }

    public boolean isThursday() {
        return (this.days & DaysOfTheWeekHelper.THURSDAY) != 0;
    }

    public void setThursday(boolean thursday) {
        if (thursday) {
            this.days += DaysOfTheWeekHelper.THURSDAY;
        } else if (isThursday()) {
            this.days -= DaysOfTheWeekHelper.THURSDAY;
        }
    }

    public boolean isFriday() {
        return (this.days & DaysOfTheWeekHelper.FRIDAY) != 0;
    }

    public void setFriday(boolean friday) {
        if (friday) {
            this.days += DaysOfTheWeekHelper.FRIDAY;
        } else if (isFriday()) {
            this.days -= DaysOfTheWeekHelper.FRIDAY;
        }
    }

    public boolean isSaturday() {
        return (this.days & DaysOfTheWeekHelper.SATURDAY) != 0;
    }

    public void setSaturday(boolean saturday) {
		if (saturday) {
			this.days += DaysOfTheWeekHelper.SATURDAY;
		} else if (isSaturday()) {
			this.days -= DaysOfTheWeekHelper.SATURDAY;
		}
    }

    public boolean isSunday() {
        return (this.days & DaysOfTheWeekHelper.SUNDAY) != 0;
    }

    public void setSunday(boolean sunday) {
		if (sunday) {
			this.days += DaysOfTheWeekHelper.SUNDAY;
		} else if (isSunday()) {
			this.days -= DaysOfTheWeekHelper.SUNDAY;
		}
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public boolean isSet() {
        return set;
    }

    public void setSet(boolean set) {
        this.set = set;
    }

    public int getSnoozeTime() {
        return snoozeTime;
    }

    public void setSnoozeTime(int snoozeTime) {
        this.snoozeTime = snoozeTime;
    }

    public boolean isSnoozeShake() {
        return snoozeShake;
    }

    public void setSnoozeShake(boolean snoozeShake) {
        this.snoozeShake = snoozeShake;
    }

    public int getMaxSnooze() {
        return maxSnooze;
    }

    public void setMaxSnooze(int maxSnooze) {
        this.maxSnooze = maxSnooze;
    }

    public boolean isVibrate() {
        return vibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.vibrate = vibrate;
    }

    public int getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(int alarmType) {
        this.alarmType = alarmType;
    }

    public boolean isVolumeProgression() {
        return volumeProgression;
    }

    public void setVolumeProgression(boolean volumeProgression) {
        this.volumeProgression = volumeProgression;
    }

    public int getAlarmDuration() {
        return alarmDuration;
    }

    public void setAlarmDuration(int alarmDuration) {
        this.alarmDuration = alarmDuration;
    }

    public int getAlarmMaxVolume() {
        return alarmMaxVolume;
    }

    public void setAlarmMaxVolume(int alarmMaxVolume) {
        this.alarmMaxVolume = alarmMaxVolume;
    }

    public Long getNextAlarm() {
        return nextAlarm;
    }

    public void setNextAlarm(Long nextAlarm) {
        this.nextAlarm = nextAlarm;
    }

    @Override
    public Alarm copy() {
        Alarm alarm = new Alarm();

        alarm.deleteAfterUse = this.deleteAfterUse;
        alarm.identifier = this.identifier;
        alarm.name = this.name;
        alarm.willRing = this.willRing;
        alarm.time = this.time;
        alarm.days = this.days;
        alarm.activated = this.activated;
        alarm.set =	this.set;
        alarm.snoozeTime = this.snoozeTime;
        alarm.snoozeShake = this.snoozeShake;
        alarm.maxSnooze = this.maxSnooze;
        alarm.vibrate = this.vibrate;
        alarm.alarmType = this.alarmType;
        alarm.volumeProgression = this.volumeProgression;
        alarm.alarmDuration = this.alarmDuration;
        alarm.alarmMaxVolume = this.alarmMaxVolume;
        alarm.nextAlarm = this.nextAlarm;

        return alarm;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (this.deleteAfterUse ? 1 : 0));
        parcel.writeInt(this.identifier);
        parcel.writeString(this.name);
        parcel.writeByte((byte) (this.willRing ? 1 : 0));
        parcel.writeLong(this.time.getTime());
        parcel.writeByte(this.days);
        parcel.writeByte((byte) (this.activated ? 1 : 0));
        parcel.writeByte((byte) (this.set ? 1 : 0));
        parcel.writeInt(this.snoozeTime);
        parcel.writeByte((byte) (this.snoozeShake ? 1 : 0));
        parcel.writeInt(this.maxSnooze);
        parcel.writeByte((byte) (this.vibrate ? 1 : 0));
        parcel.writeInt(this.alarmType);
        parcel.writeByte((byte) (this.volumeProgression ? 1 : 0));
        parcel.writeInt(this.alarmDuration);
        parcel.writeInt(this.alarmMaxVolume);
        parcel.writeLong(this.nextAlarm != null ? this.nextAlarm : -1);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Alarm createFromParcel(Parcel in) {
            return new Alarm(in);
        }

        public Alarm[] newArray(int size) {
            return new Alarm[size];
        }
    };
}
