package com.mrk.smartalarm.models;

public enum RuleMode {

	NO_RING,
	RULE_TIME,
	ENTRY_TIME

}
