package com.mrk.smartalarm.models;

import java.util.ArrayList;
import java.util.List;

public class Artist {

	private String name;

	private List<Album> albums;

	public Artist() {
		albums = new ArrayList<>();
	}

	public Artist(String name) {
		this();

		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Album> getAlbums() {
		return albums;
	}

	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}

	public void addAlbum(Album album) {
		this.albums.add(album);
	}
}
