package com.mrk.smartalarm.models.interfaces;

public interface HasDaysInterface {

	boolean isMonday();

	boolean isTuesday();

	boolean isWednesday();

	boolean isThursday();

	boolean isFriday();

	boolean isSaturday();

	boolean isSunday();

}
