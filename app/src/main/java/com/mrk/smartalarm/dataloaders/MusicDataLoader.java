package com.mrk.smartalarm.dataloaders;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.models.Album;
import com.mrk.smartalarm.models.Artist;
import com.mrk.smartalarm.models.Music;
import com.mrk.smartalarm.models.Song;
import com.mrk.smartalarm.viewmodels.MusicListViewModel;
import com.mrk.smartalarm.viewmodels.MusicViewModel;

import java.util.ArrayList;
import java.util.List;

public class MusicDataLoader {

	private List<Artist> artists;

	public List<Artist> getArtists() {
		return artists;
	}

	public void getArtistsList(Context context) {
		artists = new ArrayList<>();

		String[] columns = { MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.TITLE, MediaStore.MediaColumns.DATA };

		Cursor musicCursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, columns, null, null, null);

		if (musicCursor != null && musicCursor.moveToFirst()) {
			do {
				// artist
				Artist artist = getArtist(musicCursor.getString(0), true);

				// album
//				String albumName = musicCursor.getString();
				Long albumId = musicCursor.getLong(2);

				Album album = getAlbum(artist, albumId, true);

				if (albumId != null) {
					if (musicCursor.getString(1) != null) {
//						album.setId(musicCursor.getLong(2));
						album.setName(musicCursor.getString(1));
					}
				}

				// song
				Song song = new Song();

				if (musicCursor.getString(3) != null) {
					song.setTitle(musicCursor.getString(3));
				}

				if (musicCursor.getString(4) != null) {
					song.setFile(musicCursor.getString(4));
				}

				album.addSong(song);
			} while (musicCursor.moveToNext());

			musicCursor.close();
		}
	}

	public void updateArtistsList(MusicListViewModel listVm) {
		for (IViewModel<Music> musicVM : listVm.getViewModelList()) {
			Artist artist = getArtist(musicVM.getData().getArtist(), false);

			if (artist != null) {
				Album album = getAlbum(artist, musicVM.getData().getAlbumId(), false);

				if (album != null) {
					for (Song song : album.getSongs()) {
						if (song.getTitle().equals(musicVM.getData().getName())) {
							song.setSelected(true);
						}
					}
				}
			}
		}
	}

	public void updateViewModel(MusicListViewModel listVm) {

		for (Artist artist : this.artists) {
			for (Album album : artist.getAlbums()) {
				for (Song song : album.getSongs()) {
					boolean found = false;

					for (IViewModel<Music> musicVM : listVm.getViewModelList()) {
						found |= musicVM.getData().getArtist().equals(artist.getName())
								&& musicVM.getData().getAlbumId() == album.getId()
								&& musicVM.getData().getName().equals(song.getTitle());
					}

					MusicViewModel musicViewModel = new MusicViewModel();

					Music music = new Music();
					music.setAlbumId(album.getId());
					music.setArtist(artist.getName());
					music.setName(song.getTitle());
					music.setPath(song.getFile());

					musicViewModel.loadFromData(music);

					if (!found && song.isSelected()) {
						listVm.addVM(musicViewModel);
					} else if (found && !song.isSelected()) {
						listVm.removeVM(musicViewModel);
					}
				}
			}
		}

	}

	private Artist getArtist(String name, boolean create) {
		for (Artist artist : this.artists) {
			if (artist.getName().equals(name)) {
				return artist;
			}
		}

		Artist artist = null;

		if (create) {
			artist = new Artist(name);

			this.artists.add(artist);
		}

		return artist;
	}

	private Album getAlbum(Artist artist, Long id, boolean create) {
		for (Album album : artist.getAlbums()) {
			if (album.getId().equals(id)) {
				return album;
			}
		}

		Album album = null;

		if (create) {
			album = new Album(id);

			artist.getAlbums().add(album);
		}

		return album;
	}
}
