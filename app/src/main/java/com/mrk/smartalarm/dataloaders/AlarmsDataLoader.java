package com.mrk.smartalarm.dataloaders;

import android.content.Context;
import android.util.Xml;

import com.mrk.mrkframework.dal.IdentifierHelper;
import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.engine.helpers.DaysOfTheWeekHelper;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.models.Music;
import com.mrk.smartalarm.models.Rule;
import com.mrk.smartalarm.models.RuleMode;
import com.mrk.smartalarm.models.Silence;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.AlarmsListViewModel;
import com.mrk.smartalarm.viewmodels.RuleViewModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AlarmsDataLoader extends AbstractDataLoader<Alarm> {

	private static int FILE_VERSION = 2;
	private static final String XML_FILE = "alarms.xml";

	private List<Alarm> alarmList;
	private List<Agenda> agendaList;
	private List<Rule> ruleList;
	private List<Music> musicList;
	private List<Silence> silenceList;

	public AlarmsDataLoader(Context context) {
		alarmList = new ArrayList<>();
		agendaList = new ArrayList<>();
		ruleList = new ArrayList<>();
		musicList = new ArrayList<>();
		silenceList = new ArrayList<>();

		load(context);
	}

	@Override
	public List<Alarm> getData(Context context) {
		return this.alarmList;
	}

	public List<Agenda> getAgendasForAlarm(int alarm) {
		List<Agenda> agendas = new ArrayList<>();

		for (Agenda agenda : this.agendaList) {
			if (agenda.getAlarmIdentifier() == alarm) {
				agendas.add(agenda);
			}
		}

		return agendas;
	}

	public List<Rule> getRulesForAgenda(int agenda) {
		List<Rule> rules = new ArrayList<>();

		for (Rule rule : this.ruleList) {
			if (rule.getAgendaIdentifier() == agenda) {
				rules.add(rule);
			}
		}

		return rules;
	}

	public List<Music> getMusicForAlarm(int alarm) {
		List<Music> musics = new ArrayList<>();

		for (Music music : this.musicList) {
			if (music.getAlarmIdentifier() == alarm) {
				musics.add(music);
			}
		}

		return musics;
	}

	public List<Silence> getSilencesForAlarm(int alarm) {
		List<Silence> silences = new ArrayList<>();

		for (Silence silence : this.silenceList) {
			if (silence.isAlarmLinked() && silence.getLinkedIdentifier() == alarm) {
				silences.add(silence);
			}
		}

		return silences;
	}

	public List<Silence> getSilencesForRule(int rule) {
		List<Silence> silences = new ArrayList<>();

		for (Silence silence : this.silenceList) {
			if (!silence.isAlarmLinked() && silence.getLinkedIdentifier() == rule) {
				silences.add(silence);
			}
		}

		return silences;
	}

	private void load(Context context) {
		synchronized (this) {
			int fileVersion = 0;

			XmlPullParser parser = Xml.newPullParser();

			try {
				FileInputStream fIn = context.openFileInput(XML_FILE);
				InputStreamReader isr = new InputStreamReader(fIn);

				// auto-detect the encoding from the stream
				parser.setInput(isr);
				int eventType = parser.getEventType();

				Alarm currentAlarm = null;
				Rule currentRule = null;
				Music currentMusic = null;
				Agenda currentAgenda = null;
				Silence currentSilence = null;
				Silence currentRuleSilence = null;

				byte silenceDays = 0;

				boolean done = false;

				while (eventType != XmlPullParser.END_DOCUMENT && !done) {
					String name;

					switch (eventType) {
						case XmlPullParser.START_DOCUMENT:
							break;
						case XmlPullParser.START_TAG:
							name = parser.getName();

							if (name.equalsIgnoreCase("MRKAlarm")) {
								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("FileVersion")) {
											fileVersion = Integer.parseInt(parser.getAttributeValue(i));
										}
									}
								}
							}
							if (name.equalsIgnoreCase("Alarm")) {
								currentAlarm = new Alarm();
								currentAlarm.setTime(new Date());

								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmIdentifier")) {
											int identifier = Integer.parseInt(parser.getAttributeValue(i));
											IdentifierHelper.getInstance().setIdentifierForClass(Alarm.class, identifier);
											currentAlarm.setIdentifier(identifier);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmDeletedAfterUse")) {
											currentAlarm.setDeleteAfterUse(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmName")) {
											currentAlarm.setName(parser.getAttributeValue(i));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmHour")) {
											currentAlarm.getTime().setHours(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmMinutes")) {
											currentAlarm.getTime().setMinutes(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmTime")) {
											currentAlarm.setTime(new Date(Long.parseLong(parser.getAttributeValue(i))));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmDays")) {
											try {
												currentAlarm.setDays((byte) Integer.parseInt(parser.getAttributeValue(i)));
											} catch (NumberFormatException e) {
												currentAlarm.setMonday(parser.getAttributeValue(i).contains("mon"));
												currentAlarm.setTuesday(parser.getAttributeValue(i).contains("tue"));
												currentAlarm.setWednesday(parser.getAttributeValue(i).contains("wed"));
												currentAlarm.setThursday(parser.getAttributeValue(i).contains("thu"));
												currentAlarm.setFriday(parser.getAttributeValue(i).contains("fri"));
												currentAlarm.setSaturday(parser.getAttributeValue(i).contains("sat"));
												currentAlarm.setSunday(parser.getAttributeValue(i).contains("sun"));
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmActivated")) {
											currentAlarm.setActivated(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmSet")) {
											currentAlarm.setSet(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SnoozeDuration")) {
											currentAlarm.setSnoozeTime(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SnoozeShake")) {
											currentAlarm.setSnoozeShake(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("MaxSnoozeNumber")) {
											currentAlarm.setMaxSnooze(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmProgress")) {
											currentAlarm.setVolumeProgression(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmDuration")) {
											currentAlarm.setAlarmDuration(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmMaxVolume")) {
											currentAlarm.setAlarmMaxVolume(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmVibrate")) {
											currentAlarm.setVibrate(parser.getAttributeValue(i).equalsIgnoreCase("yes"));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmType")) {
											currentAlarm.setAlarmType(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AlarmNext")) {
											String alarmNext = parser.getAttributeValue(i);
											if (!"null".equals(alarmNext)) {
												currentAlarm.setNextAlarm(Long.parseLong(alarmNext));
											}
										}
									}
								}
							}
							if (name.equalsIgnoreCase("AlarmAgenda")) {
								currentAgenda = new Agenda();
								currentAgenda.setAlarmIdentifier(currentAlarm.getIdentifier());
								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("AgendaIdentifier")) {
											int identifier = Integer.parseInt(parser.getAttributeValue(i));
											IdentifierHelper.getInstance().setIdentifierForClass(Agenda.class, identifier);
											currentAgenda.setIdentifier(identifier);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AgendaTitle")) {
											currentAgenda.setTitle(parser.getAttributeValue(i));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("AgendaPriority")) {
											currentAgenda.setPriority(Integer.parseInt(parser.getAttributeValue(i)));
										}
									}
								}
							}
							if (name.equalsIgnoreCase("AlarmRule")) {
								currentRule = new Rule();
								currentRule.setAlarmIdentifier(currentAlarm.getIdentifier());
								currentRule.setAgendaIdentifier(currentAgenda.getIdentifier());
								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("RuleIdentifier")) {
											int identifier = Integer.parseInt(parser.getAttributeValue(i));
											IdentifierHelper.getInstance().setIdentifierForClass(Rule.class, identifier);
											currentRule.setIdentifier(identifier);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("RuleEntry")) {
											currentRule.setRegex(parser.getAttributeValue(i));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("RuleRingMode")) {
											currentRule.setMode(Enum.valueOf(RuleMode.class, parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("RuleTime")) {
											currentRule.setTime(new Date(Long.parseLong(parser.getAttributeValue(i))));
										}
									}
								}
							}
							if (name.equalsIgnoreCase("AlarmMusics")) {
								currentMusic = new Music();
								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("MusicIdentifier")) {
											int identifier = Integer.parseInt(parser.getAttributeValue(i));
											IdentifierHelper.getInstance().setIdentifierForClass(Music.class, identifier);
											currentMusic.setIdentifier(identifier);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("MusicPath")) {
											currentMusic.setPath(parser.getAttributeValue(i));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("MusicName")) {
											currentMusic.setName(parser.getAttributeValue(i));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("MusicArtist")) {
											currentMusic.setArtist(parser.getAttributeValue(i));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("MusicAlbumId")) {
											currentMusic.setAlbumId(Integer.parseInt(parser.getAttributeValue(i)));
										}
									}
								}
							}
							if (name.equalsIgnoreCase("AlarmSilence")) {
								currentSilence = new Silence();
								currentSilence.setAlarmLinked(true);
								currentSilence.setLinkedIdentifier(currentAlarm.getIdentifier());
								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceIdentifier")) {
											int identifier = Integer.parseInt(parser.getAttributeValue(i));
											IdentifierHelper.getInstance().setIdentifierForClass(Silence.class, identifier);
											currentSilence.setIdentifier(identifier);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceMode")) {
											currentSilence.setSilenceMode(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceDays")) {
											currentSilence.setDays((byte) Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceBegin")) {
											currentSilence.setBeginTime((short) Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceEnd")) {
											currentSilence.setEndTime((short) Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceBeginHour")) {
											short time = currentSilence.getBeginTime();
											time += Integer.parseInt(parser.getAttributeValue(i)) * 60;
											currentSilence.setBeginTime(time);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceBeginMinutes")) {
											short time = currentSilence.getBeginTime();
											time += Integer.parseInt(parser.getAttributeValue(i));
											currentSilence.setBeginTime(time);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceEndHour")) {
											short time = currentSilence.getEndTime();
											time += Integer.parseInt(parser.getAttributeValue(i)) * 60;
											currentSilence.setEndTime(time);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceEndMinutes")) {
											short time = currentSilence.getEndTime();
											time += Integer.parseInt(parser.getAttributeValue(i));
											currentSilence.setEndTime(time);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceMonday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.MONDAY;
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceTuesday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.TUESDAY;
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceWednesday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.WEDNESDAY;
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceThursday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.THURSDAY;
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceFriday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.FRIDAY;
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceSaturday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.SATURDAY;
											}
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceSunday")) {
											if (parser.getAttributeValue(i).equalsIgnoreCase("yes")) {
												silenceDays += DaysOfTheWeekHelper.SUNDAY;
											}
										}
									}
								}
							}
							if (name.equalsIgnoreCase("RuleSilence") && currentRule != null) {
								currentRuleSilence = new Silence();
								currentRuleSilence.setAlarmLinked(false);
								currentRuleSilence.setLinkedIdentifier(currentRule.getIdentifier());
								for (int i = 0; i < parser.getAttributeCount(); i++) {
									if (parser.getAttributeName(i) != null) {
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceIdentifier")) {
											int identifier = Integer.parseInt(parser.getAttributeValue(i));
											IdentifierHelper.getInstance().setIdentifierForClass(Silence.class, identifier);
											currentRuleSilence.setIdentifier(identifier);
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceMode")) {
											currentRuleSilence.setSilenceMode(Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceDays")) {
											currentRuleSilence.setDays((byte) Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceBegin")) {
											currentRuleSilence.setBeginTime((short) Integer.parseInt(parser.getAttributeValue(i)));
										}
										if (parser.getAttributeName(i).equalsIgnoreCase("SilenceEnd")) {
											currentRuleSilence.setEndTime((short) Integer.parseInt(parser.getAttributeValue(i)));
										}
									}
								}
							}
							break;
						case XmlPullParser.END_TAG:
							name = parser.getName();
							if (name.equalsIgnoreCase("Alarm") && currentAlarm != null) {
								alarmList.add(currentAlarm);
							}
							if (name.equalsIgnoreCase("AlarmAgenda") && currentAgenda != null) {
								this.agendaList.add(currentAgenda);
							}
							if (name.equalsIgnoreCase("AlarmRule") && currentRule != null) {
								this.ruleList.add(currentRule);
							}
							if (name.equalsIgnoreCase("AlarmMusics") && currentMusic != null) {
								this.musicList.add(currentMusic);
							}
							if (name.equalsIgnoreCase("AlarmSilence") && currentSilence != null) {
								if (silenceDays != 0) {
									currentSilence.setDays(silenceDays);
									silenceDays = 0;
								}
								this.silenceList.add(currentSilence);
							}
							if (name.equalsIgnoreCase("RuleSilence") && currentRuleSilence != null && currentRule != null) {
								this.silenceList.add(currentRuleSilence);
							}
							break;
					}
					eventType = parser.next();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}

			while (fileVersion < FILE_VERSION) {
				migrateFile(fileVersion++);
			}
		}
	}

	private void migrateFile(int fileVersion) {
		if (fileVersion == 1) {
//			for (Alarm alarmItem : alarmList) {
//			}
			for (Silence silence : this.silenceList) {
				if (silence.getBeginTime() > silence.getEndTime()) {
					// set begin time
					short beginTime = silence.getBeginTime();
					beginTime -= 1440;
					silence.setBeginTime(beginTime);

					// set mode
					int mode = silence.getSilenceMode();
					switch (mode) {
						case 2:
							// vibrate
							mode = 0;
							break;
						case 3:
							mode = 1;
							// silent
							break;
						case 0:
							// 10%
						case 1:
							// 20%
						case 4:
							// airplane
							mode = 2;
							break;
					}
					silence.setSilenceMode(mode);
				}
			}
		}
	}

	@Override
	public void pushFrom(Context context, List<Alarm> data) {
	}

	public void pushFromViewModel(final Context context, final AlarmsListViewModel data) {
		synchronized (this) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					OutputStreamWriter osw = null;

					try {
						FileOutputStream fOut = context.openFileOutput(XML_FILE, Context.MODE_PRIVATE);
						osw = new OutputStreamWriter(fOut);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

					if (osw != null) {
						String fileContent = "<?xml version='1.0' encoding='utf-8'?>\n";

						fileContent += "<MRKAlarm \tFileVersion=\"" + FILE_VERSION + "\">\n";

						fileContent += "\t<AlarmsList>";

						for (IViewModel<Alarm> alarmItem : data.getViewModelList()) {
							fileContent += "\n\t\t<Alarm ";

							fileContent += "\t AlarmIdentifier=\"" + alarmItem.getData().getIdentifier() + "\"";
							fileContent += "\t AlarmDeletedAfterUse=\"" + (alarmItem.getData().isDeleteAfterUse() ? "yes" : "no") + "\"";
							fileContent += "\t AlarmName=\"" + alarmItem.getData().getName() + "\"";
							fileContent += "\t AlarmTime=\"" + alarmItem.getData().getTime().getTime() + "\"";
							fileContent += "\t AlarmDays=\"" + alarmItem.getData().getDays() + "\"";
							fileContent += "\t AlarmActivated=\"" + (alarmItem.getData().isActivated() ? "yes" : "no") + "\"";
							fileContent += "\t AlarmSet=\"" + (alarmItem.getData().isSet() ? "yes" : "no") + "\"";
							fileContent += "\t SnoozeDuration=\"" + alarmItem.getData().getSnoozeTime() + "\"";
							fileContent += "\t SnoozeShake=\"" + (alarmItem.getData().isSnoozeShake() ? "yes" : "no") + "\"";
							fileContent += "\t MaxSnoozeNumber=\"" + alarmItem.getData().getMaxSnooze() + "\"";
							fileContent += "\t AlarmProgress=\"" + (alarmItem.getData().isVolumeProgression() ? "yes" : "no") + "\"";
							fileContent += "\t AlarmDuration=\"" + alarmItem.getData().getAlarmDuration() + "\"";
							fileContent += "\t AlarmMaxVolume=\"" + alarmItem.getData().getAlarmMaxVolume() + "\"";
							fileContent += "\t AlarmVibrate=\"" + (alarmItem.getData().isVibrate() ? "yes" : "no") + "\"";
							fileContent += "\t AlarmType=\"" + alarmItem.getData().getAlarmType() + "\"";
							fileContent += "\t AlarmNext=\"" + alarmItem.getData().getNextAlarm() + "\"";

							fileContent += ">\n";

							if (((AlarmViewModel) alarmItem).getAgendasList() != null) {

								for (IViewModel<Agenda> agendaItem : ((AlarmViewModel) alarmItem).getAgendasList().getViewModelList()) {
									fileContent += "\t\t\t<AlarmAgenda";
									fileContent += " AgendaIdentifier=\"" + agendaItem.getData().getIdentifier() + "\"";
									fileContent += " AgendaTitle=\"" + agendaItem.getData().getTitle() + "\"";
									fileContent += " AgendaPriority=\"" + agendaItem.getData().getPriority() + "\"";
									fileContent += ">\n";

									if (((AgendaViewModel) agendaItem).getRulesList() != null) {
										for (IViewModel<Rule> ruleItem : ((AgendaViewModel) agendaItem).getRulesList().getViewModelList()) {
											fileContent += "\t\t\t\t<AlarmRule";
											fileContent += " RuleIdentifier=\"" + ruleItem.getData().getIdentifier() + "\"";
											fileContent += " RuleEntry=\"" + ruleItem.getData().getRegex() + "\"";
											fileContent += " RuleRingMode=\"" + ruleItem.getData().getMode().toString() + "\"";
											fileContent += " RuleTime=\"" + ruleItem.getData().getTime().getTime() + "\"";
											fileContent += " >\n";

											for (IViewModel<Silence> silenceItem : ((RuleViewModel) ruleItem).getSilencesList().getViewModelList()) {
												fileContent += "\t\t\t\t\t<RuleSilence";
												fileContent += " SilenceIdentifier=\"" + silenceItem.getData().getIdentifier() + "\"";
												fileContent += " SilenceMode=\"" + silenceItem.getData().getSilenceMode() + "\"";
												fileContent += " SilenceDays=\"" + silenceItem.getData().getDays() + "\"";
												fileContent += " SilenceBegin=\"" + silenceItem.getData().getBeginTime() + "\"";
												fileContent += " SilenceEnd=\"" + silenceItem.getData().getEndTime() + "\"";
												fileContent += " />\n";
											}

											fileContent += "\t\t\t\t</AlarmRule>\n";
										}

									}

									fileContent += "\t\t\t\t</AlarmAgenda>\n";
								}
							}

							if (((AlarmViewModel) alarmItem).getMusicList() != null) {

								for (IViewModel<Music> musicItem : ((AlarmViewModel) alarmItem).getMusicList().getViewModelList()) {
									fileContent += "\t\t\t<AlarmMusics";
									fileContent += "\t MusicIdentifier=\"" + musicItem.getData().getIdentifier() + "\"";
									fileContent += "\t MusicPath=\"" + musicItem.getData().getPath() + "\"";
									fileContent += "\t MusicName=\"" + musicItem.getData().getName() + "\"";
									fileContent += "\t MusicArtist=\"" + musicItem.getData().getArtist() + "\"";
									fileContent += "\t MusicAlbumId=\"" + musicItem.getData().getAlbumId() + "\"";
									fileContent += "\t/>\n";
								}
							}

							if (((AlarmViewModel) alarmItem).getSilencesList() != null) {

								for (IViewModel<Silence> silenceItem : ((AlarmViewModel) alarmItem).getSilencesList().getViewModelList()) {
									fileContent += "\t\t\t<AlarmSilence";
									fileContent += "\t SilenceIdentifier=\"" + silenceItem.getData().getIdentifier() + "\"";
									fileContent += "\t SilenceMode=\"" + silenceItem.getData().getSilenceMode() + "\"";
									fileContent += "\t SilenceDays=\"" + silenceItem.getData().getDays() + "\"";
									fileContent += "\t SilenceBegin=\"" + silenceItem.getData().getBeginTime() + "\"";
									fileContent += "\t SilenceEnd=\"" + silenceItem.getData().getEndTime() + "\"";
									fileContent += "\t/>\n";
								}

							}

							fileContent += "\t\t</Alarm>\n";
						}

						fileContent += "\t</AlarmsList>";
						fileContent += "\n</MRKAlarm>";

						try {
							osw.write(fileContent);
							osw.flush();
							osw.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
		}
	}
}
