package com.mrk.smartalarm.engine.events;

public class ActivateAlarmEvent {

	private int identifier;

	private boolean activated;

	public ActivateAlarmEvent(int identifier, boolean activated) {
		this.identifier = identifier;
		this.activated = activated;
	}

	public int getIdentifier() {
		return identifier;
	}

	public boolean isActivated() {
		return activated;
	}
}
