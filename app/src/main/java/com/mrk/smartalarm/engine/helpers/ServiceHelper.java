package com.mrk.smartalarm.engine.helpers;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mrk.smartalarm.engine.pebble.PebbleCommunicationService;

public class ServiceHelper {

	public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public static ComponentName startService(Context context, Class<?> serviceClass) {
		if (serviceClass.equals(PebbleCommunicationService.class)) {
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
			if (!settings.getBoolean("pebble_vibrate_preference", false)) {
				Log.d(ServiceHelper.class.getSimpleName(), "No Pebble found");
				return null;
			}
		}

		Intent startPebbleServiceIntent = new Intent(context, serviceClass);

		return context.startService(startPebbleServiceIntent);
	}

}
