package com.mrk.smartalarm.engine.helpers;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.models.Music;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AlarmSoundHelper {

	private int previousIndex = 0;
	private String[] music;

	public AlarmSoundHelper(List<IViewModel<Music>> musicList, int alarmType) {
		int i = 0;

		this.music = new String[musicList.size()];

		for (IViewModel<Music> musicItem : musicList) {
//			if (musicItem.getData().getType() == alarmType && musicItem.getData().isFile() && new File(musicItem.getData().getPath()).exists()) {
				this.music[i++] = musicItem.getData().getPath();
//			}
		}
	}

	public String getMusic() {
		String ret;
		int newIndex = this.previousIndex;

		while (newIndex == this.previousIndex) {
			newIndex = new Random().nextInt(this.music.length - 1);
		}

		ret = this.music[newIndex];

		return ret;
	}

//	public static void checkList(Context context, Alarm alarm) {
//		ArrayList<Music> musicList = new ArrayList<Music>();
//		boolean check = false;
//
//		for (AlarmMusic musicItem : alarm.musicList) {
//			check |= (musicItem.getAlbumId() == 0);
//		}
//
//		if (check) {
//
//			for (AlarmMusic musicItem : alarm.musicList) {
//				if (musicItem.getType() != 1)
//					musicList.add(musicItem);
//			}
//
//			System.gc();
//
//			String[] proj = { MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.TITLE,
//					MediaStore.MediaColumns.DATA };
//
//			Cursor musicCursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj, null, null, null);
//
//			if (musicCursor != null) {
//
//				if (musicCursor.moveToFirst()) {
//					do {
//						String artist = musicCursor.getString(0);
//						String albumId = musicCursor.getString(2);
//						String trackName = musicCursor.getString(3);
//						String trackPath = musicCursor.getString(4);
//
//						for (AlarmMusic musicItem : alarm.musicList) {
//							if (getFileName(musicItem.getPath()).equalsIgnoreCase(getFileName(trackPath))) {
//								musicItem.setPath(trackPath);
//								musicItem.setAlbumId(Long.parseLong(albumId));
//								musicItem.setName(trackName);
//								musicItem.setArtist(artist);
//								musicList.add(musicItem);
//							}
//						}
//					} while (musicCursor.moveToNext());
//				}
//			}
//
//			if (musicList.size() != 0) {
//				alarm.musicList = musicList;
//			}
//		}
//	}

	private static String getFileName(String fullPath) {
		return fullPath.split("/")[fullPath.split("/").length - 1];
	}

	public static String getMusicFromPlaylist(String playlist) {
		FileInputStream ficRead = null;
		BufferedInputStream strRead = null;
		DataInputStream dataRead = null;
		boolean isReading = true;
		File playlistFile = new File(playlist);
		String playlistDir = playlist.replace(playlistFile.getName(), "");
		ArrayList<String> musicList = new ArrayList<String>();

		String ret = "";

		try {
			ficRead = new FileInputStream(playlistFile);
			strRead = new BufferedInputStream(ficRead);
			dataRead = new DataInputStream(strRead);
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		isReading = (dataRead != null);

		while (isReading) {
			try {
				String ligne = dataRead.readLine();
				isReading = (ligne != null);
				if (isReading) {
					if (!ligne.startsWith("#")) {
						ligne = ligne.replaceAll("\\\\", "/");
						musicList.add(ligne);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			strRead.close();
			ficRead.close();
			dataRead.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		if (musicList.size() > 0) {
			int tries = 5;
			ret = playlistDir + musicList.get(new Random().nextInt(musicList.size()));
			while (!new File(ret).exists() && tries>0) {
				ret = playlistDir + musicList.get(new Random().nextInt(musicList.size()));
				tries--;
			}
			if (tries==0)
				ret = "";
		}

		return ret;
	}

}
