package com.mrk.smartalarm.engine.events;

public class RingStopEvent {

	private boolean repeat;

	public RingStopEvent(boolean repeat) {
		this.repeat = repeat;
	}

	public boolean isRepeat() {
		return repeat;
	}
}
