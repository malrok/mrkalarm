package com.mrk.smartalarm.engine.helpers;

import com.mrk.smartalarm.models.interfaces.HasDaysInterface;

import java.util.Calendar;

public class DaysOfTheWeekHelper {

	public static final int MONDAY = 1;
	public static final int TUESDAY = 2;
	public static final int WEDNESDAY = 4;
	public static final int THURSDAY = 8;
	public static final int FRIDAY = 16;
	public static final int SATURDAY = 32;
	public static final int SUNDAY = 64;

	public static final int DAYS_OF_THE_WEEK = 31;

	public static final int WEEK_END_DAYS = 96;

	public static boolean[] getWeekArray(HasDaysInterface model) {
		boolean[] ret = { false, false, false, false, false, false, false, false };

		ret[0] = false;
		ret[Calendar.SUNDAY] = model.isSunday();
		ret[Calendar.MONDAY] = model.isMonday();
		ret[Calendar.TUESDAY] = model.isTuesday();
		ret[Calendar.WEDNESDAY] = model.isWednesday();
		ret[Calendar.THURSDAY] = model.isThursday();
		ret[Calendar.FRIDAY] = model.isFriday();
		ret[Calendar.SATURDAY] = model.isSaturday();

		return ret;
	}

}
