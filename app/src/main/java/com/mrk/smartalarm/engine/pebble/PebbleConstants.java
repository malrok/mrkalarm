package com.mrk.smartalarm.engine.pebble;

import java.util.UUID;

public class PebbleConstants {

	public static final UUID PEBBLE_APP_UUID = UUID.fromString("38301ebd-089c-48b1-ab28-5ebeeea47448");

	public static final int KEY_ALARM_NAME = 0;

	public static final int KEY_ACTION = 1;

	public static final int KEY_ALARM_ACTIVATION = 2;

	public static final int KEY_ALARMS_LIST = 10;
}
