package com.mrk.smartalarm.engine.alarm;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class VolumeSetter {

	private static final String VOLUME_SETTER_BUNDLE = "volume_setter_bundle";
	private static final String DEFAULT_VOLUME = "default_volume";
	private static final String SOUND_PROGRESS = "sound_progress";
	private static final String MAX_VOLUME = "max_volume";

	private IncreaseVolumeTask increaseVolumeTask;
	private AudioManager audioManager;

	private int defaultVolume;
	private int volumeProgressionTime;
	private float maxVolume = 15;

	public VolumeSetter(Context context) {
		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		defaultVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
	}

	public void increaseVolume(int progressTime, int maxVolume) {
		this.increaseVolumeTask = new IncreaseVolumeTask();
		this.volumeProgressionTime = progressTime;
		this.maxVolume = maxVolume;

		if (volumeProgressionTime > 0) {
			increaseVolumeFunction(0);
		}
	}

	private void increaseVolumeFunction(float currentVolume) {
		float value = maxVolume / 100;
		currentVolume += value;

		if (currentVolume <= maxVolume) {
			audioManager.setStreamVolume(AudioManager.STREAM_ALARM, (int) currentVolume, 0);
			increaseVolumeTask.sleep(currentVolume, volumeProgressionTime * 10);
		} else {
			setDefaultVolume();
		}
	}

	private void setDefaultVolume() {
		audioManager.setStreamVolume(AudioManager.STREAM_ALARM, defaultVolume, 0);
		if (increaseVolumeTask != null) {
			increaseVolumeTask.removeMessages(0);
		}
	}

	private class IncreaseVolumeTask extends Handler {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			increaseVolumeFunction(bundle.getFloat("currentVolume"));
		}

		public void sleep(float currentVolume, long delayMillis) {
			this.removeMessages(0);

			Message msg = obtainMessage(0);
			Bundle bundle = new Bundle();
			bundle.putFloat("currentVolume", currentVolume);
			msg.setData(bundle);
			sendMessageDelayed(msg, delayMillis);
		}
	}

	public void onSaveInstanceState(Bundle outState) {
		Bundle state  = new Bundle();
		state.putInt(DEFAULT_VOLUME, defaultVolume);
		state.putInt(SOUND_PROGRESS, volumeProgressionTime);
		state.putFloat(MAX_VOLUME, maxVolume);
		outState.putBundle(VOLUME_SETTER_BUNDLE, state);
	}

	public void onRestoreInstanceState(Bundle savedInstanceState) {
		Bundle state = savedInstanceState.getBundle(VOLUME_SETTER_BUNDLE);
		if (state != null) {
			defaultVolume = state.getInt(DEFAULT_VOLUME);
			volumeProgressionTime = state.getInt(SOUND_PROGRESS);
			maxVolume = state.getFloat(MAX_VOLUME);
		}
	}
}
