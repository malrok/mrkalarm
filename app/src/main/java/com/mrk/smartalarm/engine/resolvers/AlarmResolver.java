package com.mrk.smartalarm.engine.resolvers;

import android.content.Context;
import android.database.Cursor;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.models.RuleMode;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.RuleViewModel;

import java.sql.Date;
import java.util.Calendar;
import java.util.TimeZone;

public class AlarmResolver extends Resolver {

	private Context context;

	public AlarmResolver(Context context) {
		this.context = context;
	}

	public Long getNextAlarmTime(AlarmViewModel alarm) {
		Long foundAlarm = null;

		Calendar workingDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")), setDate;
		int days = 0;

		while (days < 31 && foundAlarm == null) {
			Object[] result = getNextAgendaRule(alarm, workingDate.getTimeInMillis());

			RuleViewModel ruleVM = (RuleViewModel) result[0];

			if (ruleVM == null && alarm.hasAlarmForDay(workingDate.get(Calendar.DAY_OF_WEEK))) {
				setDate = Calendar.getInstance();
				setDate.setTimeInMillis(workingDate.getTimeInMillis());
				setDate.set(Calendar.SECOND, 0);
				setDate.set(Calendar.MINUTE, alarm.getData().getTime().getMinutes());
				setDate.set(Calendar.HOUR_OF_DAY, alarm.getData().getTime().getHours());

				if (setDate.after(Calendar.getInstance())) {
					foundAlarm = setDate.getTime().getTime();
				}
			} else if (ruleVM != null && ruleVM.getData().getMode() != RuleMode.NO_RING) {
				if (ruleVM.getData().getMode() == RuleMode.ENTRY_TIME && result[1] != null) {
					foundAlarm = (Long) result[1];
				} else {
					foundAlarm = ruleVM.getData().getTime().getTime();
				}
			}

			if (foundAlarm == null){
				days++;
				workingDate.set(Calendar.HOUR_OF_DAY, 23);
				workingDate.set(Calendar.MINUTE, 59);
				workingDate.set(Calendar.SECOND, 59);
				workingDate.add(Calendar.SECOND, 1);
			}
		}

		return foundAlarm;
	}

	private Object[] getNextAgendaRule(AlarmViewModel alarmVM, long now) {
		RuleViewModel foundRule = null;

		Calendar workingDate = Calendar.getInstance();
		Calendar currentDate = null;

		Cursor cursor = getCalendarQuery(context);

		while (cursor.moveToNext()) {
			if (alarmVM.isAgendaUsed(cursor.getString(cursor.getColumnIndex("name")))) {

				Cursor eventCursor = getEventsCursor(context, cursor.getString(cursor.getColumnIndex("_id")), now);

				while (eventCursor.moveToNext()) {
					String eventTitle = eventCursor.getString(0);
					int eventStart = eventCursor.getInt(1);

					for (IViewModel<Agenda> agendaVM : alarmVM.getAgendasList().getViewModelList()) { // must be sorted by priority

						RuleViewModel ruleVM = ((AgendaViewModel)agendaVM).getRuleFromEvent(eventTitle);

						if (ruleVM != null) {
							if (ruleVM.getData().getMode() == RuleMode.RULE_TIME) {
								workingDate.set(Calendar.HOUR, ruleVM.getData().getTime().getHours());
								workingDate.set(Calendar.MINUTE, ruleVM.getData().getTime().getMinutes());
							} else if (ruleVM.getData().getMode() == RuleMode.ENTRY_TIME) {
								workingDate.setTime(new Date(eventStart));
							}

							if (currentDate == null) {
								foundRule = ruleVM;
								currentDate = Calendar.getInstance();
								currentDate.setTime(workingDate.getTime());
							} else if (currentDate.after(workingDate)) {
								foundRule = ruleVM;
								currentDate.setTime(workingDate.getTime());
							}
						}
					}
				}

				eventCursor.close();
			}
		}

		cursor.close();

		return new Object[] {foundRule, currentDate};
	}

}
