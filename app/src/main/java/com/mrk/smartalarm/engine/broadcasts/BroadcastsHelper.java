package com.mrk.smartalarm.engine.broadcasts;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.dataloaders.AlarmsDataLoader;
import com.mrk.smartalarm.engine.helpers.AlarmsHelper;
import com.mrk.smartalarm.engine.helpers.ServiceHelper;
import com.mrk.smartalarm.engine.logger.Logger;
import com.mrk.smartalarm.engine.pebble.PebbleCommunicationService;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.ui.AlarmRingScreen;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.AlarmsListViewModel;
import com.mrk.smartalarm.viewmodels.RuleViewModel;
import com.mrk.smartalarm.viewmodels.SilenceViewModel;

import java.util.Calendar;

public class BroadcastsHelper {

	public static final String ALARM_INTENT = "com.mrk.smartalarmintent";
	public static final String SILENCE_INTENT = "com.mrk.silenceintent";
	public static final String ALARM_ID = "alarm_id";
	public static final String SILENCE_ID = "silence_id";
	public static final String RULE_ID = "rule_id";
	public static final String SILENCE_START = "silence_start";
	public static final String ALARM_PARCELABLE = "alarm_parcelable";

	private static final int APP_NOTIFICATION_ID = 5672145;

	public static void onReceiveIntent(Context context, Intent intent) {
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			ServiceHelper.startService(context, PebbleCommunicationService.class);
			sendNextAlarmBroadcast(context);
			sendNextSilenceBroadcast(context);
		} else if (ALARM_INTENT.equals(intent.getAction())) {
			processAlarmIntent(context, intent);
		} else if (SILENCE_INTENT.equals(intent.getAction())) {
			processSilenceIntent(context, intent);
		}
	}

	public static void sendNextAlarmBroadcast(Context context) {
		AlarmsDataLoader dataLoader =  new AlarmsDataLoader(context);
		AlarmsListViewModel alarmsViewModel = new AlarmsListViewModel();

		alarmsViewModel.loadFromDataLoader(context, dataLoader);

		AlarmsHelper.setAlarms(context, alarmsViewModel);

		dataLoader.pushFromViewModel(context, alarmsViewModel);

		sendNextAlarmBroadcast(context, alarmsViewModel);
	}

	public static void sendNextAlarmBroadcast(Context context, AlarmsListViewModel alarms) {
		unsetAllAlarms(context, alarms);

		AlarmViewModel nextAlarm = alarms.getNextAlarm();

		if (nextAlarm != null && nextAlarm.getData().isActivated() && nextAlarm.getData().getNextAlarm() != null) {
			sendAlarmBroadcast(context, nextAlarm.getData().getIdentifier(), nextAlarm.getData().getNextAlarm());
		}
	}

	public static void sendAlarmRepeatBroadcast(Context context, AlarmViewModel alarm) {
		Calendar repeatTime = Calendar.getInstance();

		repeatTime.add(Calendar.MINUTE, alarm.getData().getSnoozeTime());

		sendAlarmBroadcast(context, alarm.getData().getIdentifier(), repeatTime.getTimeInMillis());
	}

	private static void sendAlarmBroadcast(Context context, int identifier, long time) {
		Intent intent = new Intent(context, Receiver.class);
		intent.putExtra(ALARM_ID, identifier).setAction(ALARM_INTENT);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, identifier, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		setAlarmOnManager(context, time, pendingIntent);
	}

	private static void unsetAllAlarms(Context context, AlarmsListViewModel alarms) {
		for (IViewModel<Alarm> alarm : alarms.getViewModelList()) {
			Intent intent = new Intent(context, Receiver.class);
			intent.putExtra(ALARM_ID, alarm.getData().getIdentifier()).setAction(ALARM_INTENT);

			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarm.getData().getIdentifier(), intent, 0);

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

			alarmManager.cancel(pendingIntent);
		}
	}

	/**
	 * send the next silence broadcast
	 * @param context android context
	 * @param identifier the alarm identifier
	 * @param silenceDetails the details of the silence :
	 *                       [0]: the time of the silence
	 *                       [1]: the identifier of the silence, or the identifier of the rule triggering the silence
	 *                       [2]: the identifier of the silence if it is triggered by a rule, or null
	 */
	public static void sendNextSilenceBroadcast(Context context, int identifier, Long[] silenceDetails) {
		Intent intent = new Intent(context, Receiver.class);
		intent.setAction(SILENCE_INTENT);
		intent.putExtra(ALARM_ID, identifier);
		intent.putExtra(SILENCE_START, true);

		if (silenceDetails[3] == null) {
			intent.putExtra(SILENCE_ID, silenceDetails[2]);
		} else {
			intent.putExtra(RULE_ID, silenceDetails[2]);
			intent.putExtra(SILENCE_ID, silenceDetails[3]);
		}

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 9999 - identifier, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		long pendingTime = silenceDetails[0];

		if (silenceDetails[0] <= System.currentTimeMillis() && silenceDetails[1] >= System.currentTimeMillis()) {
			pendingTime = System.currentTimeMillis();
		}

		setAlarmOnManager(context, pendingTime, pendingIntent);
	}

	public static void unsetAllSilences(Context context, AlarmsListViewModel alarms) {
		for (IViewModel<Alarm> alarm : alarms.getViewModelList()) {
			Intent intent = new Intent(context, Receiver.class);
			intent.putExtra(ALARM_ID, alarm.getData().getIdentifier()).setAction(SILENCE_INTENT);

			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 9999 - alarm.getData().getIdentifier(), intent, PendingIntent.FLAG_CANCEL_CURRENT);

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

			alarmManager.cancel(pendingIntent);
		}
		stopSilence(context, false);
	}

	private static void processAlarmIntent(Context context, Intent intent) {
		if (intent.hasExtra(ALARM_ID)) {
			AlarmsListViewModel alarmsViewModel = new AlarmsListViewModel();
			alarmsViewModel.loadFromDataLoader(context, new AlarmsDataLoader(context));

			AlarmViewModel alarmVM = alarmsViewModel.getViewModel(intent.getIntExtra(ALARM_ID , -1));

			if (alarmVM != null) {
				ringAlarm(context, alarmVM);
			}
		}
	}

	private static void ringAlarm(Context context, AlarmViewModel alarmVM) {
		Intent ringActivityIntent = new Intent(context, AlarmRingScreen.class);
		ringActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ringActivityIntent.putExtra(ALARM_PARCELABLE, alarmVM);

		context.startActivity(ringActivityIntent);
	}

	public static void sendNextSilenceBroadcast(Context context) {
		AlarmsListViewModel alarmsViewModel = new AlarmsListViewModel();
		alarmsViewModel.loadFromDataLoader(context, new AlarmsDataLoader(context));

		AlarmsHelper.setNextSilence(context, alarmsViewModel);
	}

	private static void processSilenceIntent(Context context, Intent intent) {
		if (intent.hasExtra(ALARM_ID) && intent.hasExtra(SILENCE_ID)) {
			AlarmsListViewModel alarmsViewModel = new AlarmsListViewModel();
			alarmsViewModel.loadFromDataLoader(context, new AlarmsDataLoader(context));

			AlarmViewModel alarmVM = alarmsViewModel.getViewModel(intent.getIntExtra(ALARM_ID, -1));

			if (alarmVM != null) {

				SilenceViewModel silenceVM = null;

				if (intent.hasExtra(RULE_ID)) {
					RuleViewModel ruleVM = alarmVM.getAgendasList().getRuleById((int) intent.getLongExtra(RULE_ID, -1));
					if (ruleVM != null) {
						silenceVM = ruleVM.getSilenceById((int) intent.getLongExtra(SILENCE_ID, -1));
					}
				} else {
					silenceVM = alarmVM.getSilencesList().getSilenceById((int) intent.getLongExtra(SILENCE_ID, -1));
				}

				if (silenceVM != null) {
					if (intent.getBooleanExtra(SILENCE_START, false)) {
						startSilence(context, intent, silenceVM);
					} else {
						stopSilence(context, true);
					}
				}
			}
		}
	}

	private static void stopSilence(Context context, boolean sendBroadcast) {
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		Logger.debug(context, "stopSilence", "normal " + Log.getStackTraceString(new Exception()));
		audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(APP_NOTIFICATION_ID);

		if (sendBroadcast) {
			sendNextSilenceBroadcast(context);
		}
	}

	private static void startSilence(Context context, Intent intent, SilenceViewModel silenceVM) {
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		switch (silenceVM.getData().getSilenceMode()) {
			case 0: // vibrate
				Logger.debug(context, "startSilence", "vibrate " + Log.getStackTraceString(new Exception()));
				audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
				break;
			case 1: // silent
				Logger.debug(context, "startSilence", "silent " + Log.getStackTraceString(new Exception()));
				audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
				break;
			case 2: // favorites
				// TODO
				break;
		}

		Calendar workingDate = Calendar.getInstance();
		workingDate.set(Calendar.MINUTE, silenceVM.getEndMinutes());
		workingDate.set(Calendar.HOUR_OF_DAY, silenceVM.getEndHour());

		if (workingDate.before(Calendar.getInstance())) {
			workingDate.add(Calendar.DATE, 1);
		}

		setAlarmOnManager(context, workingDate.getTimeInMillis(), getSilenceNotificationIntent(context, intent));
	}

	private static PendingIntent getSilenceNotificationIntent(Context context, Intent intent) {
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(context)
						.setSmallIcon(R.drawable.volume_off)
						.setContentTitle(context.getString(R.string.app_name))
						.setContentText(context.getString(R.string.silence_notification));

		Intent silenceIntent = new Intent(context, Receiver.class);
		silenceIntent.setAction(SILENCE_INTENT);
		silenceIntent.putExtra(SILENCE_START, false);
		silenceIntent.putExtra(ALARM_ID, intent.getIntExtra(ALARM_ID, -1));
		silenceIntent.putExtra(SILENCE_ID, intent.getLongExtra(SILENCE_ID, -1));

		if (intent.hasExtra(RULE_ID)) {
			silenceIntent.putExtra(RULE_ID, intent.getLongExtra(RULE_ID, -1));
		}

		mBuilder.setContentIntent(PendingIntent.getBroadcast(context, 0, silenceIntent, PendingIntent.FLAG_UPDATE_CURRENT));

		NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotifyMgr.notify(APP_NOTIFICATION_ID, mBuilder.build());

		return PendingIntent.getBroadcast(context, 9999 - intent.getIntExtra(ALARM_ID, -1), silenceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	private static void setAlarmOnManager(Context context, long time, PendingIntent pendingIntent) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent);
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent);
		} else {
			alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
		}
	}

}
