package com.mrk.smartalarm.engine.helpers;

import android.content.Context;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.engine.broadcasts.BroadcastsHelper;
import com.mrk.smartalarm.engine.resolvers.AlarmResolver;
import com.mrk.smartalarm.engine.resolvers.SilenceResolver;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.AlarmsListViewModel;

import java.util.Calendar;

public class AlarmsHelper {

	public static void setAlarms(Context context, AlarmsListViewModel alarmsVM) {
		AlarmResolver resolver = new AlarmResolver(context);

		for (IViewModel<Alarm> alarmVM : alarmsVM.getViewModelList()) {
			alarmVM.getData().setNextAlarm(resolver.getNextAlarmTime((AlarmViewModel) alarmVM));
		}
	}

	public static void setNextSilence(Context context, AlarmsListViewModel alarmsVM) {
		SilenceResolver resolver = new SilenceResolver(context);

		BroadcastsHelper.unsetAllSilences(context, alarmsVM);

		int foundSilence = -1;
		Long[] nextSilence = new Long[3];
//		nextSilence[0] = Calendar.getInstance().getTimeInMillis();

		for (IViewModel<Alarm> alarmVM : alarmsVM.getViewModelList()) {
			Long[] alarmSilence = resolver.getNextSilenceTime((AlarmViewModel) alarmVM);

			if (alarmVM.getData().isActivated() && alarmSilence[0] != null &&
					(alarmSilence[0] > System.currentTimeMillis() || alarmSilence[0] <= System.currentTimeMillis() && alarmSilence[1] >= System.currentTimeMillis())) {
				foundSilence = alarmVM.getData().getIdentifier();
				nextSilence = alarmSilence.clone();
			}
		}

		if (foundSilence != -1) {
			BroadcastsHelper.sendNextSilenceBroadcast(context, foundSilence, nextSilence);
		}
	}

}
