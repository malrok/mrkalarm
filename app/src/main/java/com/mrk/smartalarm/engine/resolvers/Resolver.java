package com.mrk.smartalarm.engine.resolvers;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;

public abstract class Resolver {

	protected Cursor getEventsCursor(Context context, String calendarId, long now) {
		Uri uri = Uri.parse("content://com.android.calendar/events");

		String[] projection = new String[] { CalendarContract.Events.TITLE, CalendarContract.Events.DTSTART };

		String selection = CalendarContract.Events.CALENDAR_ID + " = ? AND "
				+ "(" + CalendarContract.Events.DTSTART + " <= ? AND " + CalendarContract.Events.DTEND + "> ?)";

		String[] selectionArgs = new String[] { calendarId, Long.toString(now), Long.toString(now) };

		return context.getContentResolver().query(
				uri,
				projection,
				selection,
				selectionArgs,
				"dtstart ASC"
		);
	}

	protected Cursor getCalendarQuery(Context context) {
		Uri uri = Uri.parse("content://com.android.calendar/calendars");

		return context.getContentResolver().query(uri, new String[]{"_id", "name", "visible"}, null, null, null);
	}

}
