package com.mrk.smartalarm.engine.resolvers;

import android.content.Context;
import android.database.Cursor;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.models.Agenda;
import com.mrk.smartalarm.models.RuleMode;
import com.mrk.smartalarm.viewmodels.AgendaViewModel;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.RuleViewModel;
import com.mrk.smartalarm.viewmodels.SilenceViewModel;

import java.sql.Date;
import java.util.Calendar;

public class SilenceResolver extends Resolver {

	private Context context;

	public SilenceResolver(Context context) {
		this.context = context;
	}

	public Long[] getNextSilenceTime(AlarmViewModel alarm) {
		Long[] foundSilence = new Long[4];

		Calendar workingDate = Calendar.getInstance(), beginDate = Calendar.getInstance(), endDate = Calendar.getInstance();
		int days = 0;

		while (days < 8 && foundSilence[0] == null) {
			Object[] result = getNextAgendaRule(alarm, workingDate.getTimeInMillis());

			RuleViewModel ruleVM = (RuleViewModel) result[0];

			if (ruleVM == null && alarm.hasSilencesForDay(workingDate.get(Calendar.DAY_OF_WEEK))) {
				for (SilenceViewModel silenceVM : alarm.getSilencesForDay(workingDate.get(Calendar.DAY_OF_WEEK))) {
					beginDate.setTime(workingDate.getTime());
					beginDate.set(Calendar.MINUTE, silenceVM.getBeginMinutes());
					beginDate.set(Calendar.HOUR_OF_DAY, silenceVM.getBeginHour());

					endDate.setTime(workingDate.getTime());
					endDate.set(Calendar.MINUTE, silenceVM.getEndMinutes());
					endDate.set(Calendar.HOUR_OF_DAY, silenceVM.getEndHour());

					while (endDate.before(beginDate)) {
						endDate.add(Calendar.DATE, 1);
					}

					if (beginDate.after(Calendar.getInstance()) || beginDate.before(Calendar.getInstance()) && endDate.after(Calendar.getInstance())) {
						foundSilence[0] = beginDate.getTime().getTime();
						foundSilence[1] = endDate.getTime().getTime();
						foundSilence[2] = (long) silenceVM.getData().getIdentifier();
					}
				}
			} else if (ruleVM != null) {
				for (SilenceViewModel silenceVM : ruleVM.getSilencesList().getSilencesForDay(workingDate.get(Calendar.DAY_OF_WEEK))) {
					beginDate.setTime(workingDate.getTime());
					beginDate.set(Calendar.MINUTE, silenceVM.getBeginMinutes());
					beginDate.set(Calendar.HOUR_OF_DAY, silenceVM.getBeginHour());

					endDate.setTime(workingDate.getTime());
					endDate.set(Calendar.MINUTE, silenceVM.getEndMinutes());
					endDate.set(Calendar.HOUR_OF_DAY, silenceVM.getEndHour());

					while (endDate.before(beginDate)) {
						endDate.add(Calendar.DATE, 1);
					}

					if (beginDate.after(Calendar.getInstance()) || beginDate.before(Calendar.getInstance()) && endDate.after(Calendar.getInstance())) {
						foundSilence[0] = beginDate.getTime().getTime();
						foundSilence[1] = endDate.getTime().getTime();
						foundSilence[2] = (long) ruleVM.getData().getIdentifier();
						foundSilence[3] = (long) silenceVM.getData().getIdentifier();
					}
				}
			}

			if (foundSilence[0] == null) {
				days++;
				workingDate.set(Calendar.HOUR_OF_DAY, 23);
				workingDate.set(Calendar.MINUTE, 59);
				workingDate.set(Calendar.SECOND, 59);
				workingDate.add(Calendar.SECOND, 1);
			}
		}

		return foundSilence;
	}

	private Object[] getNextAgendaRule(AlarmViewModel alarmVM, long now) {
		RuleViewModel foundRule = null;

		Calendar workingDate = Calendar.getInstance();
		Calendar currentDate = null;

		Cursor cursor = getCalendarQuery(context);

		while (cursor.moveToNext()) {
			if (alarmVM.isAgendaUsed(cursor.getString(cursor.getColumnIndex("name")))) {

				Cursor eventCursor = getEventsCursor(context, cursor.getString(cursor.getColumnIndex("_id")), now);

				while (eventCursor.moveToNext()) {
					String eventTitle = eventCursor.getString(0);
					int eventStart = eventCursor.getInt(1);

					for (IViewModel<Agenda> agendaVM : alarmVM.getAgendasList().getViewModelList()) { // must be sorted by priority

						RuleViewModel ruleVM = ((AgendaViewModel)agendaVM).getRuleFromEvent(eventTitle);

						if (ruleVM != null) {
							if (ruleVM.getData().getMode() == RuleMode.RULE_TIME) {
								workingDate.set(Calendar.HOUR, ruleVM.getData().getTime().getHours());
								workingDate.set(Calendar.MINUTE, ruleVM.getData().getTime().getMinutes());
							} else if (ruleVM.getData().getMode() == RuleMode.ENTRY_TIME) {
								workingDate.setTime(new Date(eventStart));
							}

							if (currentDate == null) {
								foundRule = ruleVM;
								currentDate = Calendar.getInstance();
								currentDate.setTime(workingDate.getTime());
							} else if (currentDate.after(workingDate)) {
								foundRule = ruleVM;
								currentDate.setTime(workingDate.getTime());
							}
						}
					}
				}

				eventCursor.close();
			}
		}

		cursor.close();

		return new Object[] {foundRule, currentDate};
	}

}
