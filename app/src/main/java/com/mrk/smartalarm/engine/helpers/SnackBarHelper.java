package com.mrk.smartalarm.engine.helpers;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.mrk.smartalarm.R;

public class SnackBarHelper {

	public static Snackbar make(Activity activity, int messId, int duration) {
		return make(activity.findViewById(android.R.id.content), messId, duration);
	}

	public static Snackbar make(View view, int messId, int duration) {
		Snackbar snack = Snackbar.make(view, messId, duration);
		snack.getView().setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
		snack.setActionTextColor(ContextCompat.getColor(view.getContext(), android.R.color.white));
		return snack;
	}

}
