package com.mrk.smartalarm.engine.logger;

import android.content.Context;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Logger {

	private static final String DEBUG_FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/smartalarm.log";

	public static void debug(Context context, String TAG, String log) {
		if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("debug_preference", false)) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(DEBUG_FILE, true));

				writer.print(new SimpleDateFormat("MM/dd/yy hh:mm:ss z", Locale.FRANCE).format(new Date()));
				writer.print(" ");
				writer.print(TAG);
				writer.print(" ");
				writer.print(Thread.currentThread().getName());
				writer.print(" - ");
				writer.println(log);

				writer.flush();
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void debug(Exception exception) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(new FileWriter(DEBUG_FILE, true));
			exception.printStackTrace(writer);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
