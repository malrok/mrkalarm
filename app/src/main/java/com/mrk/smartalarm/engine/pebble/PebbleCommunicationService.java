package com.mrk.smartalarm.engine.pebble;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.smartalarm.dataloaders.AlarmsDataLoader;
import com.mrk.smartalarm.engine.broadcasts.BroadcastsHelper;
import com.mrk.smartalarm.engine.events.ActivateAlarmEvent;
import com.mrk.smartalarm.engine.events.RingStartEvent;
import com.mrk.smartalarm.engine.events.RingStopEvent;
import com.mrk.smartalarm.engine.helpers.AlarmsHelper;
import com.mrk.smartalarm.engine.helpers.BytesArrayHelper;
import com.mrk.smartalarm.models.Alarm;
import com.mrk.smartalarm.viewmodels.AlarmViewModel;
import com.mrk.smartalarm.viewmodels.AlarmsListViewModel;

import de.greenrobot.event.EventBus;

public class PebbleCommunicationService extends Service {

	private static final int SLEEP_LENGTH = 1000;

	private AlarmViewModel alarmVM;

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		EventBus.getDefault().register(this);
		PebbleKit.registerReceivedDataHandler(this, pebbleDataReceiver);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

	// EventBus callback
	public void onEvent(RingStartEvent event) {
		this.alarmVM = event.getViewModel();

		PebbleKit.startAppOnPebble(getApplicationContext(), PebbleConstants.PEBBLE_APP_UUID);
	}

	private PebbleKit.PebbleDataReceiver pebbleDataReceiver = new PebbleKit.PebbleDataReceiver(PebbleConstants.PEBBLE_APP_UUID) {

		@Override
		public void receiveData(final Context context, final int transactionId, final PebbleDictionary received) {
			if (received.contains(PebbleConstants.KEY_ACTION)) {
				PebbleKit.sendAckToPebble(getApplicationContext(), transactionId);

				PebbleKit.closeAppOnPebble(getApplicationContext(), PebbleConstants.PEBBLE_APP_UUID);

				EventBus.getDefault().post(new RingStopEvent(received.getInteger(PebbleConstants.KEY_ACTION) == 0));
			} else if (received.contains(PebbleConstants.KEY_ALARM_NAME)) {
				PebbleKit.sendAckToPebble(getApplicationContext(), transactionId);

				sleep(SLEEP_LENGTH);

				PebbleDictionary sent = new PebbleDictionary();
				sent.addString(PebbleConstants.KEY_ALARM_NAME, alarmVM.getData().getName());
				PebbleKit.sendDataToPebble(getApplicationContext(), PebbleConstants.PEBBLE_APP_UUID, sent);
			} else if (received.contains(PebbleConstants.KEY_ALARMS_LIST)) {
				PebbleKit.sendAckToPebble(getApplicationContext(), transactionId);

				sleep(SLEEP_LENGTH);

				AlarmsListViewModel alarmsViewModel = new AlarmsListViewModel();
				alarmsViewModel.loadFromDataLoader(context, new AlarmsDataLoader(context));

				PebbleDictionary sent = new PebbleDictionary();

				BytesArrayHelper arrayHelper = new BytesArrayHelper();

				arrayHelper.addBytesToArray(false, new byte[]{(byte) alarmsViewModel.getViewModelList().size()});

				Long nextDate = -1L;

				if (alarmsViewModel.getNextAlarm() != null) {
					nextDate = alarmsViewModel.getNextAlarm().getData().getNextAlarm();
				}

				arrayHelper.addLongToArray(false, nextDate);

				sent.addBytes(PebbleConstants.KEY_ALARMS_LIST, arrayHelper.getBytesArray());

				int key = PebbleConstants.KEY_ALARMS_LIST + 1;

				for (IViewModel<Alarm> vm : alarmsViewModel.getViewModelList()) {
					arrayHelper = new BytesArrayHelper();

					arrayHelper.addLongToArray(false, vm.getData().getTime().getTime());
					arrayHelper.addBytesToArray(false, new byte[]{(byte) (vm.getData().isActivated() ? 1 : 0)});
					arrayHelper.addBytesToArray(false, new byte[]{(byte) vm.getData().getIdentifier()});
					arrayHelper.addStringToArray(true, vm.getData().getName());

					sent.addBytes(key++, arrayHelper.getBytesArray());
				}
				PebbleKit.sendDataToPebble(getApplicationContext(), PebbleConstants.PEBBLE_APP_UUID, sent);
			} else if (received.contains(PebbleConstants.KEY_ALARM_ACTIVATION)) {
				PebbleKit.sendAckToPebble(getApplicationContext(), transactionId);

				Long id = received.getInteger(PebbleConstants.KEY_ALARM_ACTIVATION);

				if (id != null) {
					AlarmsDataLoader dataLoader = new AlarmsDataLoader(context);
					AlarmsListViewModel alarmsViewModel = new AlarmsListViewModel();
					alarmsViewModel.loadFromDataLoader(context, dataLoader);

					IViewModel<Alarm> vm = alarmsViewModel.getViewModel(id.intValue());

					if (vm != null) {
						vm.getData().setActivated(!vm.getData().isActivated());

						// set next alarm
						AlarmsHelper.setAlarms(context, alarmsViewModel);
						BroadcastsHelper.sendNextAlarmBroadcast(context, alarmsViewModel);

						// set next silence
						AlarmsHelper.setNextSilence(context, alarmsViewModel);

						dataLoader.pushFromViewModel(context, alarmsViewModel);

						EventBus.getDefault().post(new ActivateAlarmEvent(vm.getData().getIdentifier(), vm.getData().isActivated()));

						PebbleDictionary sent = new PebbleDictionary();

						BytesArrayHelper arrayHelper = new BytesArrayHelper();

						Long nextDate = -1L;

						if (alarmsViewModel.getNextAlarm() != null) {
							nextDate = alarmsViewModel.getNextAlarm().getData().getNextAlarm();
						}

						arrayHelper.addLongToArray(false, nextDate);
						arrayHelper.addBytesToArray(false, new byte[]{(byte) (vm.getData().isActivated() ? 1 : 0)});
						arrayHelper.addBytesToArray(false, new byte[]{(byte) vm.getData().getIdentifier()});

						sent.addBytes(PebbleConstants.KEY_ALARM_ACTIVATION, arrayHelper.getBytesArray());

						PebbleKit.sendDataToPebble(getApplicationContext(), PebbleConstants.PEBBLE_APP_UUID, sent);
					}
				}
			}
		}

	};

	private void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
