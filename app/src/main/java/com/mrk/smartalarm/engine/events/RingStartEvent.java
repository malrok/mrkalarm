package com.mrk.smartalarm.engine.events;

import com.mrk.smartalarm.viewmodels.AlarmViewModel;

public class RingStartEvent {

	private AlarmViewModel viewModel;

	public RingStartEvent(AlarmViewModel viewModel) {
		this.viewModel = viewModel;
	}

	public AlarmViewModel getViewModel() {
		return viewModel;
	}
}
