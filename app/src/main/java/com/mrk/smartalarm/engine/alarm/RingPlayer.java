package com.mrk.smartalarm.engine.alarm;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

public class RingPlayer implements Serializable {

	public static final String MUSIC_COMPLETED = "com.mrk.musiccompleted";

	public MediaPlayer startPlayer(final Context context, int soundType, String file, boolean repeat) {
		MediaPlayer player = new MediaPlayer();

		if (file.length() > 0) {
			try {
				if (soundType == 1 || soundType == 2) {
					player.setDataSource(new FileInputStream(file).getFD());
				} else {
					player.setDataSource(context, Uri.parse(file));
				}

				player.setOnCompletionListener(new OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer mp) {
						Intent intent = new Intent();
						intent.setAction(MUSIC_COMPLETED);
						context.sendBroadcast(intent);
					}
				});
				player.setAudioStreamType(AudioManager.STREAM_ALARM);
				player.setLooping(repeat);

				player.prepare();
				player.start();
			} catch (Exception e) {
				player = playFallback(context);
			}
		} else {
			player = playFallback(context);
		}

		return player;
	}

	@NonNull
	private MediaPlayer playFallback(Context context) {
		MediaPlayer player = new MediaPlayer();

		Uri path = Uri.parse("android.resource://" + context.getApplicationContext().getPackageName() + "/raw/alarm_beep");

		try {
			player.setDataSource(context, path);
			player.setAudioStreamType(AudioManager.STREAM_ALARM);
			player.setLooping(true);
			player.prepare();
			player.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return player;
	}

	public void stop(MediaPlayer player) {
		if (player != null) {
			if (player.isPlaying()) {
				player.stop();
			}
			player.release();
		}
	}
}
