package com.mrk.smartalarm.engine.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;

import com.mrk.smartalarm.R;

public class ConfirmationHelper {

	public interface ConfirmationListener {
		void confirm();

		void reject();
	}

	public static void askConfirmation(Context context, int title, int message, final ConfirmationListener listener) {
		askConfirmation(context,context.getString(title), context.getString(message), listener);
	}

	public static void askConfirmation(Context context, String title, String message, final ConfirmationListener listener) {
		// set builder
		AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.DialogStyle));

		// set it up
		builder
			.setTitle(title)
			.setMessage(message)
			.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					listener.confirm();
					dialogInterface.cancel();
				}
			})
			.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					listener.reject();
					dialogInterface.cancel();
				}
			});

		// create alert dialog
		AlertDialog alertDialog = builder.create();

		// show it
		alertDialog.show();
	}

}
