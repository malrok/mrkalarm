package com.mrk.smartalarm.engine.helpers;

import android.content.Context;

public class TimeHelper {

	public static String getFormattedTime(Context context, int hour, int minutes) {
		String ret = "";

		if (!android.text.format.DateFormat.is24HourFormat(context)) {
			if (hour > 12)
				ret = ((hour - 12 < 10) ? "0" + (hour - 12) : hour - 12) + ":" + ((minutes < 10) ? "0" + minutes : minutes) + " pm";
			else if (hour < 12 && hour > 0)
				ret = ((hour < 10) ? "0" + hour : hour) + ":" + ((minutes < 10) ? "0" + minutes : minutes) + " am";
			else if (hour == 0)
				ret = "12:" + ((minutes < 10) ? "0" + minutes : minutes) + " am";
			else if (hour == 12)
				ret = "12:" + ((minutes < 10) ? "0" + minutes : minutes) + " pm";
		} else {
			ret = ((hour < 10) ? "0" + hour : (hour == 24 ? "00" : hour) ) + ":" + ((minutes < 10) ? "0" + minutes : minutes);
		}

		return ret;
	}

}
