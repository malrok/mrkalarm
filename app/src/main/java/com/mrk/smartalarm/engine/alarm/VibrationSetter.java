package com.mrk.smartalarm.engine.alarm;

import android.content.Context;
import android.os.Vibrator;

public class VibrationSetter {

	private Vibrator vibrator;

	private long[] pattern = { 0, 200, 500 };

	public VibrationSetter(Context context) {
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
	}

	public void startVibration() {
		vibrator.vibrate(pattern, 0);
	}

	private void stopVibration() {
		if (vibrator != null)
			vibrator.cancel();
	}
}
