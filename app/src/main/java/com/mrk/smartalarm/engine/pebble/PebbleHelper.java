package com.mrk.smartalarm.engine.pebble;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;

import com.getpebble.android.kit.PebbleKit;
import com.mrk.smartalarm.R;
import com.mrk.smartalarm.engine.helpers.ConfirmationHelper;
import com.mrk.smartalarm.engine.helpers.SnackBarHelper;

public class PebbleHelper {

	public static final String FIRST_LAUNCH_PREFERENCE = "firstlaunch_preference";

	public static void checkPebbleOnFirstLaunch(final Activity activity, final PebbleHelperCallback callback) {
		final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());

		if (sharedPreferences.getBoolean(FIRST_LAUNCH_PREFERENCE, true)) {
			if (PebbleKit.isWatchConnected(activity.getApplicationContext())) {
				ConfirmationHelper.askConfirmation(activity, R.string.pebble_dialog_title, R.string.pebble_dialog_message,
					new ConfirmationHelper.ConfirmationListener() {
						@Override
						public void confirm() {
							sharedPreferences.edit().putBoolean("pebble_vibrate_preference", true).apply();
							callback.onPebbleLinked();
						}

						@Override
						public void reject() {
							SnackBarHelper.make(activity, R.string.change_from_settings, Snackbar.LENGTH_SHORT).show();
						}
					}
				);
			}
			sharedPreferences.edit().putBoolean(FIRST_LAUNCH_PREFERENCE, false).apply();
		} else {
			if (sharedPreferences.getBoolean("pebble_vibrate_preference", false)) {
				callback.onPebbleLinked();
			}
		}
	}

	public interface PebbleHelperCallback {

		void onPebbleLinked();

	}

}
