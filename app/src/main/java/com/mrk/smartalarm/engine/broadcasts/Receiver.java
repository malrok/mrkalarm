package com.mrk.smartalarm.engine.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Receiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		BroadcastsHelper.onReceiveIntent(context, intent);
	}

}
