package com.mrk.smartalarm.engine.helpers;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;

import com.mrk.smartalarm.R;

import java.util.ArrayList;
import java.util.List;

public class CalendarHelper {

	public static String[] getCalendarsList(Activity activity) {
		List<String> calList = new ArrayList<>();
		Cursor managedCursor;

		String[] projection = new String[] { "_id", "name" };

		Uri calendars = Uri.parse("content://com.android.calendar/calendars");

		managedCursor = activity.getContentResolver().query(calendars, projection, null, null, null);

		if (managedCursor != null) {
			if (managedCursor.moveToFirst()) {
				int nameColumn = managedCursor.getColumnIndex("name");
				int idColumn = managedCursor.getColumnIndex("_id");

				String name;
				String id;

				do {
					name = managedCursor.getString(nameColumn);
					id = managedCursor.getString(idColumn);
					if (name != null && id != null) {
						calList.add(name);
					}
				} while (managedCursor.moveToNext());

			}

			managedCursor.close();
		}

		if (calList.isEmpty()) {
			calList.add(activity.getString(R.string.no_calendar_found));
		}

		String[] res = new String[calList.size()];

		for (int i=0; i<calList.size(); i++) {
			res[i] = calList.get(i);
		}

		return res;
	}

}
